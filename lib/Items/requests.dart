// ignore_for_file: camel_case_types

import 'package:ceylon_crafts/sizeConfig.dart';
import 'package:flutter/material.dart';


class requests extends StatefulWidget {
  const requests({ Key? key }) : super(key: key);

  @override
  _requestsState createState() => _requestsState();
}

class _requestsState extends State<requests> {
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return const Scaffold(
      body: SafeArea(child: Center(
        child: Text('Requests'),
      )),
    );
  }
}