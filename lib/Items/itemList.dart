// ignore_for_file: file_names

import 'package:flutter/material.dart';

import 'item.dart';

List<item> itemlist = [

  item(
    itemName: "Cotton Pillows", 
    createdDate: "2 weeks ago", 
    location: "Kandy", 
    description: "Cotton pillows",
    stock: 10,
    price: 1000,
    images: ['assets/shopImages/pillows.png']
    ),
  
  item(
    itemName: "Sri Lankan Wooden masks", 
    createdDate: "1 week ago", 
    location: "Ambalangoda", 
    description: "Sri lankan Wooden Masks",
    stock: 5,
    price: 850,
    images: ['assets/shopImages/mask.png']
    ),

  item(
    itemName: "Pastal Painting", 
    createdDate: "3 weeks ago", 
    location: "Warakapola", 
    description: "Pastal Painting",
    stock: 4,
    price: 1500,
    images: ['assets/shopImages/paintings.png']
    ),

  item(
    itemName: "Clay Pot", 
    createdDate: "2 weeks ago", 
    location: "Kandy", 
    description: "Clay Pot",
    stock: 8,
    price: 900,
    images: ['assets/shopImages/clay_item.png']
    ),

  item(
    itemName: "Bathik Clothes", 
    createdDate: "2 days ago", 
    location: "Kegalle",
    description: "Bathik clothes",
    stock: 5,
    price: 500,
    images: ['assets/shopImages/bathik.jpg']
    ),

  item(
    itemName: "Wool Decoration", 
    createdDate: "5 days ago", 
    location: "Horana", 
    description: "Wool decorations",
    stock: 20,
    price: 900,
    images: ['assets/shopImages/wool_design.png']
    ),

];