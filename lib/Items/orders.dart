// ignore_for_file: camel_case_types

import 'package:ceylon_crafts/sizeConfig.dart';
import 'package:flutter/material.dart';


class orders extends StatefulWidget {
  const orders({ Key? key }) : super(key: key);

  @override
  _ordersState createState() => _ordersState();
}

class _ordersState extends State<orders> {
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return const Scaffold(
      body: SafeArea(child: Center(
        child: Text('Orders'),
      )),
    );
  }
}