// ignore_for_file: camel_case_types

import 'package:ceylon_crafts/sizeConfig.dart';
import 'package:flutter/material.dart';


class courses extends StatefulWidget {
  const courses({ Key? key }) : super(key: key);

  @override
  _coursesState createState() => _coursesState();
}

class _coursesState extends State<courses> {
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return const Scaffold(
      body: SafeArea(child: Center(
        child: Text('Courses'),
      )),
    );
  }
}