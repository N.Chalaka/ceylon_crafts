// ignore_for_file: file_names, camel_case_types

import 'package:flutter/material.dart';

class item{
  String itemName = "";
  String createdDate = "";
  String location = "";
  String description = "";
  int stock = 0;
  double price = 0.0;
  List<String> images =[];
  

  item({required this.itemName,required this.createdDate,required this.location,required this.description,required this.stock, required this.price,required this.images});


  
}