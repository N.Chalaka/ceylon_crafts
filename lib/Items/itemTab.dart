// ignore_for_file: file_names, camel_case_types

import 'package:ceylon_crafts/Customer/customBoxShadow.dart';
import 'package:ceylon_crafts/Items/itemPage.dart';
import 'package:ceylon_crafts/Items/account(h).dart';
import 'package:ceylon_crafts/sizeConfig.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import 'courses.dart';
import 'orders.dart';
import 'requests.dart';

class itemTab extends StatefulWidget {
  const itemTab({ Key? key }) : super(key: key);

  @override
  _itemTabState createState() => _itemTabState();
}

class _itemTabState extends State<itemTab> with SingleTickerProviderStateMixin{
  late TabController _tabController;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: 5, vsync: this);
    _tabController.addListener(_handleTabSelection);
    
  }

  void _handleTabSelection() {
    setState(() {
     // widget.col = Colors.green;
    });
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return DefaultTabController(
        length: 5,
        child: Scaffold(
          extendBodyBehindAppBar: true,
          appBar: PreferredSize(
            preferredSize: Size.fromHeight(SizeConfig.blockSizeVertical * 16),
            child: Container(
              width: SizeConfig.screenWidth,
              height: SizeConfig.blockSizeVertical * 16,
              decoration: BoxDecoration(
                 color: Colors.white,
                
                  borderRadius: BorderRadius.only(
                    bottomLeft:
                        Radius.circular(SizeConfig.blockSizeVertical * 3),
                    bottomRight:
                        Radius.circular(SizeConfig.blockSizeVertical * 3),
                  ),
                  boxShadow: [
                    CustomBoxShadow(
                        color: Colors.grey.withOpacity(0.5),
                        blurRadius: 2,
                        spreadRadius: 3,
                        offset: const Offset(0, 3))
                  ]),
              child: TabBar(
                controller: _tabController,
                padding: EdgeInsets.only(bottom: SizeConfig.blockSizeVertical,top: SizeConfig.safeAreaVertical),
              
                isScrollable: false,
                indicatorColor: Colors.transparent,
                labelPadding: EdgeInsets.only(
                    left: SizeConfig.blockSizeHorizontal,
                    right: SizeConfig.blockSizeHorizontal,
                  //  bottom: SizeConfig.blockSizeVertical,
                   top: SizeConfig.blockSizeVertical
                   
                    ),
                labelColor: Colors.green,
                unselectedLabelColor: Colors.brown,
                labelStyle: TextStyle(
                  fontSize: SizeConfig.blockSizeVertical * 2,
                  
                ),
                //padding: const EdgeInsets.symmetric(horizontal: 0),
                tabs: [
                  Tab(
                   height: SizeConfig.blockSizeVertical*16,
                  iconMargin: const EdgeInsets.only(bottom: 0,),
                    icon: SvgPicture.asset(
                      'assets/icons/items.svg',
                      width: SizeConfig.blockSizeVertical * 5,
                      height: SizeConfig.blockSizeVertical * 5,
                    //  fit: BoxFit.contain,
                      color: _tabController.index == 0
                          ? Colors.green
                          : Colors.brown,
                    ),
                    text: 'Items'
                   
                     
                    
                      
                  ),
                  Tab(
                    height: SizeConfig.blockSizeVertical*16,
                    iconMargin: const EdgeInsets.only(bottom: 0,),
                    icon: SvgPicture.asset(
                      'assets/icons/orders.svg',
                      width: SizeConfig.blockSizeVertical * 5,
                      height: SizeConfig.blockSizeVertical * 5,
                     // fit: BoxFit.cover,
                      color: _tabController.index == 1
                          ? Colors.green
                          : Colors.brown,
                    ),
                    text: 'orders',
                  ),
                  Tab(
                    height: SizeConfig.blockSizeVertical*16,
                   iconMargin: const EdgeInsets.only(bottom: 0,),
                    icon: SvgPicture.asset(
                      'assets/icons/requests.svg',
                      width: SizeConfig.blockSizeVertical * 5,
                      height: SizeConfig.blockSizeVertical * 5,
                     // fit: BoxFit.cover,
                      color: _tabController.index == 2
                          ? Colors.green
                          : Colors.brown,
                    ),
                    text: 'Requests',
                  ),
                  Tab(
                    height: SizeConfig.blockSizeVertical*16,
                   iconMargin: const EdgeInsets.only(bottom: 0,),
                    icon: SvgPicture.asset(
                      'assets/icons/courses.svg',
                      width: SizeConfig.blockSizeVertical * 5,
                      height: SizeConfig.blockSizeVertical * 5,
                     // fit: BoxFit.cover,
                      color: _tabController.index == 3
                          ? Colors.green
                          : Colors.brown,
                    ),
                    text: 'Courses',
                  ),
                  Tab(
                    height: SizeConfig.blockSizeVertical*16,
                    iconMargin: const EdgeInsets.only(bottom: 0,),
                    icon: SvgPicture.asset(
                      'assets/icons/account(h).svg',
                      width: SizeConfig.blockSizeVertical * 5,
                      height: SizeConfig.blockSizeVertical * 5,
                      //fit: BoxFit.cover,
                      color: _tabController.index == 4
                          ? Colors.green
                          : Colors.brown,
                    ),
                    text: 'Account',
                  ),
                ],
              ),
            ),
          ),
          body: TabBarView(
            controller: _tabController,
          children: const [
            itemPage(),
            orders(),
            requests(),
            courses(),
            handi_account()
          ]
          
          ),
        ));
  }
}
