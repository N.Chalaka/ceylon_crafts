// ignore_for_file: file_names, camel_case_types

import 'package:ceylon_crafts/sizeConfig.dart';
import 'package:flutter/material.dart';

class handi_account extends StatefulWidget {
  const handi_account({ Key? key }) : super(key: key);

  @override
  _handi_accountState createState() => _handi_accountState();
}

class _handi_accountState extends State<handi_account> {
  @override
  @override
   Widget build(BuildContext context) {
    SizeConfig().init(context);
    return const Scaffold(
      body: SafeArea(child: Center(
        child: Text('Account'),
      )),
    );
  }
}