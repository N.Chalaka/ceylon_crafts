// ignore: file_names
// ignore_for_file: file_names, prefer_const_literals_to_create_immutables

import 'package:ceylon_crafts/Items/item.dart';
import 'package:ceylon_crafts/Items/itemBuilder.dart';
import 'package:ceylon_crafts/Items/itemList.dart';
import 'package:ceylon_crafts/searchWidget.dart';
import 'package:ceylon_crafts/sizeConfig.dart';
import 'package:flutter/material.dart';



class itemPage extends StatefulWidget {
  const itemPage({ Key? key }) : super(key: key);

  @override
  _itemPageState createState() => _itemPageState();
}

class _itemPageState extends State<itemPage> {

  String query = '';
  late List<item> items;
  

  @override
  void initState(){
    super.initState();
    items = itemlist;
  }

  void deleteItem(item,itemlist){
    //itemlist.remove(item);
    setState(() {
      itemlist.remove(item);
    });
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return GestureDetector(
      onTap: (){
        FocusScopeNode currentFocus = FocusScope.of(context);
        if(!currentFocus.hasPrimaryFocus){
          currentFocus.unfocus();
          
        }
      },
      child: Scaffold(
        body: SafeArea(
          child: Column(
            children: [
              
              Container(
                margin: EdgeInsets.only(top:SizeConfig.blockSizeVertical*2),
                width: SizeConfig.blockSizeHorizontal*80,
               // height: SizeConfig.blockSizeVertical*5,
                child: buildSearch()
                ),
              Expanded(
                child: ListView.builder(
                   itemCount: items.length,
                   itemBuilder: (context, index) {
                     final item = items[index];
                     return itemBuilder(i: item,onDelete: ()=> deleteItem(item,items));
                   }
                   
                   ),
              ),
            ],
          ),
             
          ),
          floatingActionButton: Container(
            width: SizeConfig.blockSizeVertical*10,
            height: SizeConfig.blockSizeVertical*10,
            child: FittedBox(
              child: FloatingActionButton(
                backgroundColor: Colors.brown,
                
                child: Column(
                  children: [
                    Icon(Icons.add,color: Colors.white,size:35,),
                    Text(
                      'ADD',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 10
                      ),
                      )
                  ],),
                onPressed: (){
                  
                },
                ),
            ),
          ),
            
      ),
    );
  }

  Widget buildSearch() {
  return searchWidget(
      text: query,
       onChanged: searchItems,
        hintText: 'Search items here..',
        bordercolor: Colors.grey,
        );
}

void searchItems(String query){
  final itemslist = itemlist.where((item){
    final  nameLower = item.itemName.toLowerCase();
    final  locationLower = item.location.toLowerCase();
    final  searchLower = query.toLowerCase();

    return  nameLower.contains(searchLower)||
      locationLower.contains(searchLower);
  }).toList();

  setState(() {
    this.query = query;
    items = itemslist;
  });

}

}

