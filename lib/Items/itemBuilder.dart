// ignore_for_file: file_names, camel_case_types


import 'package:ceylon_crafts/Items/item.dart';
import 'package:ceylon_crafts/sizeConfig.dart';
import 'package:flutter/material.dart';






class itemBuilder extends StatefulWidget {
  final VoidCallback onDelete;
  final item i;
  const itemBuilder(
    { Key? key,
      required this.i,
      required this.onDelete
   }) : super(key: key);

  @override
  _testitemBuilderState createState() => _testitemBuilderState();
}

class _testitemBuilderState extends State<itemBuilder> with AutomaticKeepAliveClientMixin {

  
  
  @override
  Widget build(BuildContext context) {
    super.build(context);
    SizeConfig().init(context);
    return Padding(
    padding: EdgeInsets.fromLTRB(SizeConfig.blockSizeHorizontal, SizeConfig.blockSizeVertical*0.5, SizeConfig.blockSizeHorizontal, SizeConfig.blockSizeVertical*0.5),
    child: Card(
      elevation: 0,
      shape: RoundedRectangleBorder(
           side:  BorderSide(color: Colors.grey.withOpacity(0.3),width: 2),
           borderRadius: BorderRadius.circular(SizeConfig.blockSizeVertical*2)
         ),
        //shadowColor: Colors.grey,
      //height: SizeConfig.blockSizeVertical * 20,
     
    /* decoration: BoxDecoration(
        color: Colors.transparent,
        borderRadius:BorderRadius.circular(SizeConfig.blockSizeVertical*2),
      /*  boxShadow: const [BoxShadow(
          color: Colors.white,
          spreadRadius: 1,
        )]*/
         ),*/
      child: InkWell(
        splashColor:Colors.grey.shade300,
            onTap: (){
              
            },
        child: Container(
          width: SizeConfig.blockSizeHorizontal * 90,
       // color: Colors.white,
        
         // color: Colors.white,
          child: Row(
           //mainAxisAlignment: MainAxisAlignment.spaceBetween,
            //crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: EdgeInsets.only(left: SizeConfig.blockSizeHorizontal * 2),
                child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Container(
                      height: SizeConfig.blockSizeHorizontal * 18,
                      width: SizeConfig.blockSizeHorizontal * 18,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(
                              SizeConfig.blockSizeVertical ),
                              
                              ),
                      child: Image.asset(widget.i.images[0]),
                    )
                  ],
                ),
              ),
              Column(
                //mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: EdgeInsets.only(
                        top: SizeConfig.blockSizeVertical * 3,
                        left: SizeConfig.blockSizeHorizontal * 4),
                    child: Container(
                     
                      width: SizeConfig.blockSizeHorizontal * 44,
                      child: Wrap(
                        children: 
                          [Text(
                            widget.i.itemName,
                            style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.w400,
                                fontSize: SizeConfig.blockSizeVertical * 2.5),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                      top: SizeConfig.blockSizeVertical * 2,
                      bottom: SizeConfig.blockSizeVertical*1,
                      left: SizeConfig.blockSizeHorizontal * 4,
                    ),
                    child: Container(
                      width: SizeConfig.blockSizeHorizontal * 44,
                      child: Text(
                        widget.i.createdDate,
                        style: TextStyle(
                          color: Colors.grey,
                          fontSize: SizeConfig.blockSizeVertical * 2,
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                        top: SizeConfig.blockSizeVertical *1,
                        bottom: SizeConfig.blockSizeVertical*3,
                        left: SizeConfig.blockSizeHorizontal * 4,
                        ),
                    child: Container(
                      height: SizeConfig.blockSizeVertical * 3,
                     // width: SizeConfig.blockSizeHorizontal * 20,
                      decoration: BoxDecoration(
                          borderRadius:
                              BorderRadius.circular(SizeConfig.blockSizeVertical),
                          color: Colors.brown),
                      child: Padding(
                        padding:
                            EdgeInsets.fromLTRB(SizeConfig.blockSizeHorizontal*2, SizeConfig.blockSizeVertical*0.1, SizeConfig.blockSizeHorizontal*2, SizeConfig.blockSizeVertical*0.1),
                        child: Text(
                          widget.i.location,
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: SizeConfig.blockSizeVertical * 2,
                          ),
                        ),
                      ),
                    ),
                  ),
                  // Padding(
                  //   padding: EdgeInsets.only(
                  //       top: SizeConfig.blockSizeVertical * 1.5,
                  //       bottom: SizeConfig.blockSizeVertical * 1.5,
                  //       left: SizeConfig.blockSizeHorizontal * 4),
                  //   child: Container(
                  //       width: SizeConfig.blockSizeHorizontal * 60,
                  //       child: RatingBarIndicator(
                  //         itemBuilder: (context, index) {
                  //           return const Icon(
                  //             Icons.star,
                  //             color: Colors.amber,
                  //           );
                  //         },
                  //         rating: ,
                  //         unratedColor: Colors.amber.withOpacity(0.2),
                  //         itemSize: SizeConfig.blockSizeVertical * 3,
                  //       )),
                  // ),
                ],
              ),
              Column(
                
              // mainAxisAlignment: MainAxisAlignment.spaceBetween,
               // mainAxisSize: MainAxisSize.min,
                children: [

                  Padding(
                    padding: EdgeInsets.only(left: SizeConfig.blockSizeHorizontal*3),
                    child: Container(
                      alignment: Alignment.topCenter,
                      width: SizeConfig.blockSizeHorizontal*22,
                    //height: SizeConfig.blockSizeVertical*18,
                    
                    child: Column(
                      //mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          width: SizeConfig.blockSizeHorizontal*22,
                         // height: SizeConfig.blockSizeVertical*20,
                          alignment: Alignment.topRight,
                          child: PopupMenuButton(
                          offset: Offset(SizeConfig.blockSizeHorizontal*3,-2),
                          shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(SizeConfig.blockSizeVertical*2),
                                side: const BorderSide(color: Colors.brown,width: 2)
                                ),
                                padding: const EdgeInsets.all(0),
                              icon: Icon(Icons.more_horiz,size: SizeConfig.blockSizeHorizontal*5,),
                              itemBuilder: (BuildContext context){
                                return[
                                    PopupMenuItem(
                                    height: SizeConfig.blockSizeVertical,
                                    
                                    enabled: true,
                                    child: Padding(
                                      padding:const EdgeInsets.only(bottom: 0,top: 0),
                                      child: ListTile(
                                        leading: Icon(Icons.edit,color: Colors.brown,size: SizeConfig.blockSizeVertical*3.5,),
                                        title: Text('Edit',style: TextStyle(fontSize: SizeConfig.blockSizeVertical*2),),
                                        minLeadingWidth: SizeConfig.blockSizeHorizontal*0.5,
                                        minVerticalPadding: 0,
                                        horizontalTitleGap: SizeConfig.blockSizeHorizontal*3,
                                        contentPadding: const EdgeInsets.all(0),
                                        ),
                                    ),
                                  ),
                                   PopupMenuItem(
                                    height: SizeConfig.blockSizeVertical,
                                    onTap: widget.onDelete,
                                    enabled: true,
                                    child: Padding(
                                      padding: const EdgeInsets.only(bottom: 0,top: 0),
                                      child: ListTile(
                                        leading: Icon(Icons.delete,color: Colors.brown,size: SizeConfig.blockSizeVertical*3.5,),
                                        title: Text('Delete',style: TextStyle(fontSize: SizeConfig.blockSizeVertical*2),),
                                        minLeadingWidth: SizeConfig.blockSizeHorizontal*0.5,
                                        minVerticalPadding: 0,
                                        horizontalTitleGap: SizeConfig.blockSizeHorizontal*3,
                                        contentPadding: const EdgeInsets.all(0),
                                        ),
                                    ),
                                  ),
                                ];
                              },
                              ),

                        ),
                  
                    Container(
                   
                      width: SizeConfig.blockSizeHorizontal*22,
                      height: SizeConfig.blockSizeVertical*9,
                      alignment: Alignment.bottomCenter,
                     // height: SizeConfig.blockSizeVertical*20,
                    
                      child: Padding(
                        padding: EdgeInsets.only(bottom:SizeConfig.blockSizeVertical),
                        child: Text(
                          'Rs. ${widget.i.price}',
                          style: TextStyle(color: Colors.green,fontSize: SizeConfig.blockSizeVertical*2),
                          ),
                      )
                    )
                ]
                    )
                    ),
                  )
                ],
              )
            ],
          ),
          //shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(SizeConfig.blockSizeVertical*2),),
        ),
      ),
    ),
  );
  }

  @override
  
  bool get wantKeepAlive => true;
}



