import 'Item.dart';

List<Item> list = [
  Item(
      itemName: "Wooden mask",
      createdDate: "2 weeks ago",
      location: "Kandy",
      description: " Ornamental wooden mask",
      stock: 10,
      status: "Pending",
      images: ['assets/itemImages/mask1.jpg', 'assets/itemImages/mask2.jpg']),
  Item(
      itemName: "Clay Pot",
      createdDate: "3 weeks ago",
      location: "Kegalle",
      description:
          "Clay pot made of high quality clay.High quality clay pot for low and reasonable and affordable price",
      stock: 12,
      status: "Ended",
      images: [
        'assets/itemImages/pot1.png',
        'assets/itemImages/pot2.png',
        'assets/itemImages/pot3.png',
      ]),
  Item(
      itemName: "Wood Design",
      createdDate: "2 weeks ago",
      location: "Kandy",
      description: " Ornamental wooden mask",
      stock: 10,
      status: "Reject",
      images: [
        'assets/itemImages/elephant.jpg',
      ]),
];
