// ignore_for_file: camel_case_types

import 'package:ceylon_crafts/Customer/customer_pages/sub_pages/account/order history/Item.dart';
import 'package:ceylon_crafts/Customer/customer_pages/sub_pages/account/order history/item_details.dart';
import 'package:ceylon_crafts/Customer/customer_pages/sub_pages/account/order history/itembuilder.dart';
import 'package:ceylon_crafts/Customer/customer_pages/sub_pages/account/order history/itemlist.dart';
import 'package:ceylon_crafts/sizeConfig.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class order_history extends StatefulWidget {
  const order_history({Key? key}) : super(key: key);

  @override
  _order_historyState createState() => _order_historyState();
}

class _order_historyState extends State<order_history> {
  late List<Item> items;

  @override
  void initState() {
    super.initState();
    items = list;
  }

  void deleteItem(item, itemlist) {
    //itemlist.remove(item);
    setState(() {
      itemlist.remove(item);
    });
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      body: SafeArea(
        child: Container(
          width: SizeConfig.screenWidth,
          height: SizeConfig.screenHeight - SizeConfig.safeAreaVertical,
          color: Colors.white,
          child: Stack(children: [
            Positioned(
              child: SvgPicture.asset(
                'assets/side-image.svg',
                height: SizeConfig.screenWidth / 2,
                width: SizeConfig.screenWidth / 2,
              ),
              top: 0,
              right: 0,
            ),
            Column(
              children: [
                Padding(
                  padding:
                      EdgeInsets.only(bottom: SizeConfig.blockSizeVertical * 1),
                  child: Container(
                    //padding: EdgeInsets.only(left:SizeConfig.blockSizeHorizontal*5),
                    height: SizeConfig.blockSizeVertical * 4,
                    width: SizeConfig.screenWidth,

                    alignment: Alignment.centerLeft,
                    child: IconButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      icon: const Icon(Icons.arrow_back_ios),
                      color: Colors.brown,
                      iconSize: SizeConfig.blockSizeVertical * 3,
                    ),
                  ),
                ),
                Column(
                  children: [
                    Padding(
                      padding: EdgeInsets.only(
                          top: SizeConfig.blockSizeHorizontal * 1,
                          bottom: SizeConfig.blockSizeHorizontal * 1),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          SvgPicture.asset(
                            'assets/icons/order_history.svg',
                            width: SizeConfig.blockSizeVertical * 4,
                            height: SizeConfig.blockSizeVertical * 4,
                            color: Colors.brown,
                            fit: BoxFit.contain,
                          ),
                          Padding(
                            padding: EdgeInsets.only(
                                left: SizeConfig.blockSizeHorizontal * 2),
                            child: Text(
                              'Order History',
                              style: TextStyle(
                                  color: Colors.brown,
                                  fontSize: SizeConfig.blockSizeVertical * 2.5),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
                Expanded(
                  child: ListView.builder(
                      itemCount: items.length,
                      itemBuilder: (context, index) {
                        final item = items[index];
                        return item_builder(
                          item: item,
                          ontap: () => displayDetails(item),
                        );
                      }),
                ),
              ],
            ),
          ]),
        ),
      ),
    );
  }

  Widget itemDetails(Item item) {
    return Container(
        width: SizeConfig.blockSizeHorizontal * 80,
        height: SizeConfig.blockSizeVertical * 80,
        child: SingleChildScrollView(
          child: Column(
            children: [
              SizedBox(
                height: SizeConfig.blockSizeVertical * 5,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    IconButton(
                      onPressed: () {
                        Navigator.of(context).pop(context);
                      },
                      icon: Padding(
                        padding: EdgeInsets.only(
                            top: SizeConfig.blockSizeVertical * 0.5,
                            right: SizeConfig.blockSizeHorizontal * 2),
                        child: const Icon(Icons.close),
                      ),
                      color: Colors.grey,
                      iconSize: SizeConfig.blockSizeVertical * 4,
                    ),
                  ],
                ),
              ),
              item_details(item: item),
            ],
          ),
        ));
  }

  void displayDetails(Item item) {
    showModalBottomSheet(
        context: context,
        builder: ((builder) => itemDetails(item)),
        isScrollControlled: true,
        backgroundColor: Colors.white,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(SizeConfig.blockSizeVertical * 3),
                topRight: Radius.circular(SizeConfig.blockSizeVertical * 3))));
    // setState(() {
    //    showModalBottomSheet(context: context,builder: ((builder)=>itemDetails()));
    // });
  }
}
