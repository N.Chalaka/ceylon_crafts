import 'package:flutter/material.dart';

class Item {
  String itemName = "";
  String createdDate = "";
  String location = "";
  String description = "";
  int stock = 0;
  String status = "";
  List<String> images = [];

  Item(
      {required this.itemName,
      required this.createdDate,
      required this.location,
      required this.description,
      required this.stock,
      required this.status,
      required this.images});
}
