import 'dart:ui';

import 'package:ceylon_crafts/sizeConfig.dart';
import 'package:flutter/material.dart';

import 'Item.dart';

class item_builder extends StatefulWidget {
  final Item item;
  final VoidCallback ontap;

  const item_builder({Key? key, required this.item, required this.ontap})
      : super(key: key);

  @override
  _item_builderState createState() => _item_builderState();
}

class _item_builderState extends State<item_builder> {
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    bool _flag = true;
    String _flag1 = "Pending";

    var _hasBeenPressed;
    return Padding(
      padding: EdgeInsets.fromLTRB(
          SizeConfig.blockSizeHorizontal,
          SizeConfig.blockSizeVertical * 0.5,
          SizeConfig.blockSizeHorizontal,
          SizeConfig.blockSizeVertical * 0.5),
      child: Card(
        elevation: 0,
        shape: RoundedRectangleBorder(
            side: BorderSide(color: Colors.grey.withOpacity(0.3), width: 2),
            borderRadius:
                BorderRadius.circular(SizeConfig.blockSizeVertical * 2)),
        //shadowColor: Colors.grey,
        //height: SizeConfig.blockSizeVertical * 20,

        /* decoration: BoxDecoration(
        color: Colors.transparent,
        borderRadius:BorderRadius.circular(SizeConfig.blockSizeVertical*2),
      /*  boxShadow: const [BoxShadow(
          color: Colors.white,
          spreadRadius: 1,
        )]*/
         ),*/
        child: InkWell(
          splashColor: Colors.grey.shade300,
          onTap:
              //Navigator.of(context).push(MaterialPageRoute(builder: (context)=> description(item: widget.item,)));
              //Navigator.of(context).pushNamed("details",arguments: widget.item);
              widget.ontap,
          child: Container(
            width: SizeConfig.blockSizeHorizontal * 90,
            // color: Colors.white,

            // color: Colors.white,
            child: Row(
              //mainAxisAlignment: MainAxisAlignment.spaceBetween,
              //crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding:
                      EdgeInsets.only(left: SizeConfig.blockSizeHorizontal * 2),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Container(
                          height: SizeConfig.blockSizeHorizontal * 18,
                          width: SizeConfig.blockSizeHorizontal * 18,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(
                                SizeConfig.blockSizeVertical),
                          ),
                          child: Image.asset(widget.item.images[0]))
                    ],
                  ),
                ),
                Column(
                  //mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: EdgeInsets.only(
                          top: SizeConfig.blockSizeVertical * 3,
                          left: SizeConfig.blockSizeHorizontal * 4),
                      child: Container(
                        width: SizeConfig.blockSizeHorizontal * 44,
                        child: Wrap(
                          children: [
                            Text(
                              widget.item.itemName,
                              style: TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.w400,
                                  fontSize: SizeConfig.blockSizeVertical * 2.5),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(
                        top: SizeConfig.blockSizeVertical * 2,
                        bottom: SizeConfig.blockSizeVertical * 1,
                        left: SizeConfig.blockSizeHorizontal * 4,
                      ),
                      child: Container(
                        width: SizeConfig.blockSizeHorizontal * 44,
                        child: Text(
                          widget.item.createdDate,
                          style: TextStyle(
                            color: Colors.grey,
                            fontSize: SizeConfig.blockSizeVertical * 2,
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(
                        top: SizeConfig.blockSizeVertical * 1,
                        bottom: SizeConfig.blockSizeVertical * 3,
                        left: SizeConfig.blockSizeHorizontal * 4,
                      ),
                      child: Container(
                        height: SizeConfig.blockSizeVertical * 3,
                        // width: SizeConfig.blockSizeHorizontal * 20,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(
                                SizeConfig.blockSizeVertical),
                            color: Colors.brown),
                        child: Padding(
                          padding: EdgeInsets.fromLTRB(
                              SizeConfig.blockSizeHorizontal * 2,
                              SizeConfig.blockSizeVertical * 0.1,
                              SizeConfig.blockSizeHorizontal * 2,
                              SizeConfig.blockSizeVertical * 0.1),
                          child: Text(
                            widget.item.location,
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: SizeConfig.blockSizeVertical * 2,
                            ),
                          ),
                        ),
                      ),
                    ),
                    // Padding(
                    //   padding: EdgeInsets.only(
                    //       top: SizeConfig.blockSizeVertical * 1.5,
                    //       bottom: SizeConfig.blockSizeVertical * 1.5,
                    //       left: SizeConfig.blockSizeHorizontal * 4),
                    //   child: Container(
                    //       width: SizeConfig.blockSizeHorizontal * 60,
                    //       child: RatingBarIndicator(
                    //         itemBuilder: (context, index) {
                    //           return const Icon(
                    //             Icons.star,
                    //             color: Colors.amber,
                    //           );
                    //         },
                    //         rating: ,
                    //         unratedColor: Colors.amber.withOpacity(0.2),
                    //         itemSize: SizeConfig.blockSizeVertical * 3,
                    //       )),
                    // ),
                  ],
                ),
                Column(
                  // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  // mainAxisSize: MainAxisSize.min,
                  children: [
                    Padding(
                      padding: EdgeInsets.only(
                          left: SizeConfig.blockSizeHorizontal * 3),
                      child: Container(
                          alignment: Alignment.topCenter,
                          width: SizeConfig.blockSizeHorizontal * 22,
                          //height: SizeConfig.blockSizeVertical*18,

                          child: Column(
                              //mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Container(
                                  width: SizeConfig.blockSizeHorizontal * 22,
                                  // height: SizeConfig.blockSizeVertical*20,
                                  alignment: Alignment.topRight,
                                  child: PopupMenuButton(
                                    offset: Offset(
                                        SizeConfig.blockSizeHorizontal * 3, -2),
                                    shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(
                                            SizeConfig.blockSizeVertical * 2),
                                        side: BorderSide(
                                            color: Colors.brown, width: 2)),
                                    padding: EdgeInsets.all(0),
                                    icon: Icon(
                                      Icons.more_horiz,
                                      size: SizeConfig.blockSizeHorizontal * 5,
                                    ),
                                    itemBuilder: (BuildContext context) {
                                      return [
                                        PopupMenuItem(
                                          height: SizeConfig.blockSizeVertical,
                                          enabled: true,
                                          child: Padding(
                                            padding: EdgeInsets.only(
                                                bottom: 0, top: 0),
                                            child: ListTile(
                                              leading: Icon(
                                                Icons.edit,
                                                color: Colors.brown,
                                                size: SizeConfig
                                                        .blockSizeVertical *
                                                    3.5,
                                              ),
                                              title: Text(
                                                'Edit',
                                                style: TextStyle(
                                                    fontSize: SizeConfig
                                                            .blockSizeVertical *
                                                        2),
                                              ),
                                              minLeadingWidth: SizeConfig
                                                      .blockSizeHorizontal *
                                                  0.5,
                                              minVerticalPadding: 0,
                                              horizontalTitleGap: SizeConfig
                                                      .blockSizeHorizontal *
                                                  3,
                                              contentPadding: EdgeInsets.all(0),
                                            ),
                                          ),
                                        ),
                                        PopupMenuItem(
                                          height: SizeConfig.blockSizeVertical,
                                          //onTap: widget.onDelete,
                                          enabled: true,
                                          child: Padding(
                                            padding: EdgeInsets.only(
                                                bottom: 0, top: 0),
                                            child: ListTile(
                                              leading: Icon(
                                                Icons.delete,
                                                color: Colors.brown,
                                                size: SizeConfig
                                                        .blockSizeVertical *
                                                    3.5,
                                              ),
                                              title: Text(
                                                'Delete',
                                                style: TextStyle(
                                                    fontSize: SizeConfig
                                                            .blockSizeVertical *
                                                        2),
                                              ),
                                              minLeadingWidth: SizeConfig
                                                      .blockSizeHorizontal *
                                                  0.5,
                                              minVerticalPadding: 0,
                                              horizontalTitleGap: SizeConfig
                                                      .blockSizeHorizontal *
                                                  3,
                                              contentPadding: EdgeInsets.all(0),
                                            ),
                                          ),
                                        ),
                                      ];
                                    },
                                  ),
                                ),
                                Container(
                                  width: SizeConfig.blockSizeHorizontal * 60,
                                  height: SizeConfig.blockSizeVertical * 9,
                                  alignment: Alignment.bottomCenter,
                                  // height: SizeConfig.blockSizeVertical*20,

                                  child: ElevatedButton(
                                    onPressed: () {},
                                    child: Text(widget.item.status),
                                    style: ElevatedButton.styleFrom(
                                      primary: Colors.green[
                                          700], //change background color of button
                                      onPrimary: Colors
                                          .white, //change text color of button
                                      shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(20),
                                      ),
                                    ),
                                  ),
                                )
                              ])),
                    )
                  ],
                )
              ],
            ),
            //shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(SizeConfig.blockSizeVertical*2),),
          ),
        ),
      ),
    );
  }
}
