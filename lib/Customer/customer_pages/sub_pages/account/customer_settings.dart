// ignore_for_file: camel_case_types

import 'package:ceylon_crafts/sizeConfig.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class customer_settings extends StatefulWidget {
  const customer_settings({Key? key}) : super(key: key);

  @override
  _customer_settingsState createState() => _customer_settingsState();
}

class _customer_settingsState extends State<customer_settings> {
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);

    return Scaffold(
      body: SafeArea(
        child: Container(
          width: SizeConfig.screenWidth,
          height: SizeConfig.screenHeight - SizeConfig.safeAreaVertical,
          color: Colors.white,
          child: Stack(children: [
            Positioned(
              child: SvgPicture.asset(
                'assets/side-image.svg',
                height: SizeConfig.screenWidth / 2,
                width: SizeConfig.screenWidth / 2,
              ),
              top: 0,
              right: 0,
            ),
            Column(
              children: [
                Padding(
                  padding:
                      EdgeInsets.only(bottom: SizeConfig.blockSizeVertical * 1),
                  child: Container(
                    //padding: EdgeInsets.only(left:SizeConfig.blockSizeHorizontal*5),
                    height: SizeConfig.blockSizeVertical * 4,
                    width: SizeConfig.screenWidth,

                    alignment: Alignment.centerLeft,
                    child: IconButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      icon: const Icon(Icons.arrow_back_ios),
                      color: Colors.brown,
                      iconSize: SizeConfig.blockSizeVertical * 3,
                    ),
                  ),
                ),
                Column(
                  children: [
                    Padding(
                      padding: EdgeInsets.only(
                          top: SizeConfig.blockSizeHorizontal * 1,
                          bottom: SizeConfig.blockSizeHorizontal * 1),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          SvgPicture.asset(
                            'assets/icons/setting.svg',
                            width: SizeConfig.blockSizeVertical * 4,
                            height: SizeConfig.blockSizeVertical * 4,
                            color: Colors.brown,
                            fit: BoxFit.contain,
                          ),
                          Padding(
                            padding: EdgeInsets.only(
                                left: SizeConfig.blockSizeHorizontal * 2),
                            child: Text(
                              'Settings',
                              style: TextStyle(
                                  color: Colors.brown,
                                  fontSize: SizeConfig.blockSizeVertical * 2.5),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
                Expanded(
                  child: Center(
                    child: Column(
                      children: [
                        Padding(
                          padding: EdgeInsets.only(
                              top: SizeConfig.blockSizeHorizontal * 4),
                          child: Container(
                            width: SizeConfig.blockSizeHorizontal * 80,
                            height: SizeConfig.blockSizeVertical * 73,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(
                                    SizeConfig.blockSizeVertical * 2),
                                border:
                                    Border.all(color: Colors.brown, width: 3)),
                            child: Column(
                              children: [
                                Row(
                                  children: [
                                    Padding(
                                      padding: EdgeInsets.only(
                                        left:
                                            SizeConfig.blockSizeHorizontal * 8,
                                        top:
                                            SizeConfig.blockSizeHorizontal * 10,
                                      ),
                                      child: SvgPicture.asset(
                                        'assets/icons/language.svg',
                                        width: SizeConfig.blockSizeVertical * 4,
                                        height:
                                            SizeConfig.blockSizeVertical * 4,
                                        color: Colors.brown,
                                        fit: BoxFit.contain,
                                      ),
                                    ),
                                    Padding(
                                        padding: EdgeInsets.only(
                                          left: SizeConfig.blockSizeHorizontal *
                                              1,
                                          top: SizeConfig.blockSizeHorizontal *
                                              10,
                                        ),
                                        child: PopupMenuButton(
                                          icon: const Icon(Icons.more_vert),
                                          itemBuilder: (BuildContext context) =>
                                              <PopupMenuEntry>[
                                            const PopupMenuItem(
                                                child: Text('English')),
                                            const PopupMenuItem(
                                                child: Text('Sinhala')),
                                          ],
                                        )),
                                    Padding(
                                      padding: EdgeInsets.only(
                                        left:
                                            SizeConfig.blockSizeHorizontal * 3,
                                        top:
                                            SizeConfig.blockSizeHorizontal * 10,
                                      ),
                                      child: Text(
                                        'Language',
                                        style: TextStyle(
                                            color: Colors.brown,
                                            fontSize:
                                                SizeConfig.blockSizeVertical *
                                                    2.0,
                                            fontWeight: FontWeight.w500),
                                      ),
                                    ),
                                  ],
                                ),
                                const Divider(
                                  color: Colors.brown,
                                  height: 50,
                                  thickness: 2,
                                  indent: 20,
                                  endIndent: 20,
                                ),
                                Row(
                                  children: [
                                    Padding(
                                      padding: EdgeInsets.only(
                                        left:
                                            SizeConfig.blockSizeHorizontal * 8,
                                      ),
                                      child: SvgPicture.asset(
                                        'assets/icons/dark mode.svg',
                                        width: SizeConfig.blockSizeVertical * 4,
                                        height:
                                            SizeConfig.blockSizeVertical * 4,
                                        color: Colors.brown,
                                        fit: BoxFit.contain,
                                      ),
                                    ),
                                    Padding(
                                      padding: EdgeInsets.only(
                                        left:
                                            SizeConfig.blockSizeHorizontal * 2,
                                      ),
                                      child: Text(
                                        'Dark Mode',
                                        style: TextStyle(
                                            color: Colors.brown,
                                            fontSize:
                                                SizeConfig.blockSizeVertical *
                                                    2.0,
                                            fontWeight: FontWeight.w500),
                                      ),
                                    ),
                                    Padding(
                                      padding: EdgeInsets.only(
                                        left:
                                            SizeConfig.blockSizeHorizontal * 10,
                                      ),
                                      child: const Icon(Icons.toggle_on),
                                    ),
                                  ],
                                ),
                                const Divider(
                                  color: Colors.brown,
                                  height: 50,
                                  thickness: 2,
                                  indent: 20,
                                  endIndent: 20,
                                ),
                                Row(
                                  children: [
                                    Padding(
                                      padding: EdgeInsets.only(
                                        left:
                                            SizeConfig.blockSizeHorizontal * 8,
                                      ),
                                      child: SvgPicture.asset(
                                        'assets/icons/appSetting.svg',
                                        width: SizeConfig.blockSizeVertical * 4,
                                        height:
                                            SizeConfig.blockSizeVertical * 4,
                                        color: Colors.brown,
                                        fit: BoxFit.contain,
                                      ),
                                    ),
                                    Padding(
                                      padding: EdgeInsets.only(
                                        left:
                                            SizeConfig.blockSizeHorizontal * 2,
                                      ),
                                      child: Text(
                                        'App Information',
                                        style: TextStyle(
                                            color: Colors.brown,
                                            fontSize:
                                                SizeConfig.blockSizeVertical *
                                                    2.0,
                                            fontWeight: FontWeight.w500),
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ]),
        ),
      ),
    );
  }
}
