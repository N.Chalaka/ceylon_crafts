// ignore_for_file: camel_case_types

import 'package:ceylon_crafts/sizeConfig.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class help extends StatefulWidget {
  const help({Key? key}) : super(key: key);

  @override
  _helpState createState() => _helpState();
}

class _helpState extends State<help> {
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      body: SafeArea(
        child: Container(
          width: SizeConfig.screenWidth,
          height: SizeConfig.screenHeight - SizeConfig.safeAreaVertical,
          color: Colors.white,
          child: Stack(children: [
            Positioned(
              child: SvgPicture.asset(
                'assets/side-image.svg',
                height: SizeConfig.screenWidth / 2,
                width: SizeConfig.screenWidth / 2,
              ),
              top: 0,
              right: 0,
            ),
            Column(
              children: [
                Padding(
                  padding:
                      EdgeInsets.only(bottom: SizeConfig.blockSizeVertical * 1),
                  child: Container(
                    //padding: EdgeInsets.only(left:SizeConfig.blockSizeHorizontal*5),
                    height: SizeConfig.blockSizeVertical * 4,
                    width: SizeConfig.screenWidth,

                    alignment: Alignment.centerLeft,
                    child: IconButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      icon: const Icon(Icons.arrow_back_ios),
                      color: Colors.brown,
                      iconSize: SizeConfig.blockSizeVertical * 3,
                    ),
                  ),
                ),
                Column(
                  children: [
                    Padding(
                      padding: EdgeInsets.only(
                          top: SizeConfig.blockSizeHorizontal * 1,
                          bottom: SizeConfig.blockSizeHorizontal * 1),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          SvgPicture.asset(
                            'assets/icons/help.svg',
                            width: SizeConfig.blockSizeVertical * 4,
                            height: SizeConfig.blockSizeVertical * 4,
                            color: Colors.brown,
                            fit: BoxFit.contain,
                          ),
                          Padding(
                            padding: EdgeInsets.only(
                                left: SizeConfig.blockSizeHorizontal * 2),
                            child: Text(
                              'Help',
                              style: TextStyle(
                                  color: Colors.brown,
                                  fontSize: SizeConfig.blockSizeVertical * 2.5),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
                Expanded(
                  child: Center(
                    child: Column(
                      children: [
                        Padding(
                          padding: EdgeInsets.only(
                              top: SizeConfig.blockSizeHorizontal * 4),
                          child: Container(
                            width: SizeConfig.blockSizeHorizontal * 80,
                            height: SizeConfig.blockSizeVertical * 65,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(
                                    SizeConfig.blockSizeVertical * 2),
                                border:
                                    Border.all(color: Colors.brown, width: 3)),
                            child: Column(
                              children: [
                                Row(
                                  children: [
                                    Padding(
                                      padding: EdgeInsets.only(
                                        left:
                                            SizeConfig.blockSizeHorizontal * 8,
                                        top:
                                            SizeConfig.blockSizeHorizontal * 10,
                                      ),
                                      child: SvgPicture.asset(
                                        'assets/icons/people.svg',
                                        width: SizeConfig.blockSizeVertical * 4,
                                        height:
                                            SizeConfig.blockSizeVertical * 4,
                                        color: Colors.brown,
                                        fit: BoxFit.contain,
                                      ),
                                    ),
                                    Padding(
                                      padding: EdgeInsets.only(
                                        left:
                                            SizeConfig.blockSizeHorizontal * 1,
                                        top:
                                            SizeConfig.blockSizeHorizontal * 10,
                                      ),
                                      child: Text(
                                        'Contact us',
                                        style: TextStyle(
                                            color: Colors.brown,
                                            fontSize:
                                                SizeConfig.blockSizeVertical *
                                                    2.5,
                                            fontWeight: FontWeight.w500),
                                      ),
                                    ),
                                  ],
                                ),
                                Row(
                                  children: [
                                    Padding(
                                      padding: EdgeInsets.only(
                                        left:
                                            SizeConfig.blockSizeHorizontal * 12,
                                        top:
                                            SizeConfig.blockSizeHorizontal * 10,
                                      ),
                                      child: Text(
                                        'Location: \n Ceylon Carft, \n Matara, \n Sri Lanka ',
                                        style: TextStyle(
                                            color: Colors.brown,
                                            fontSize:
                                                SizeConfig.blockSizeVertical *
                                                    2.0,
                                            fontWeight: FontWeight.w500),
                                      ),
                                    ),
                                    Padding(
                                      padding: EdgeInsets.only(
                                        left:
                                            SizeConfig.blockSizeHorizontal * 10,
                                        top:
                                            SizeConfig.blockSizeHorizontal * 10,
                                      ),
                                      child: SvgPicture.asset(
                                        'assets/icons/location.svg',
                                        width: SizeConfig.blockSizeVertical * 4,
                                        height:
                                            SizeConfig.blockSizeVertical * 4,
                                        color: Colors.brown,
                                        fit: BoxFit.contain,
                                      ),
                                    ),
                                  ],
                                ),
                                Row(
                                  children: [
                                    Padding(
                                      padding: EdgeInsets.only(
                                        left:
                                            SizeConfig.blockSizeHorizontal * 12,
                                        top:
                                            SizeConfig.blockSizeHorizontal * 10,
                                      ),
                                      child: Text(
                                        'Contact No : \n  +947758962 \n +943325489',
                                        style: TextStyle(
                                            color: Colors.brown,
                                            fontSize:
                                                SizeConfig.blockSizeVertical *
                                                    2.0,
                                            fontWeight: FontWeight.w500),
                                      ),
                                    ),
                                    Padding(
                                      padding: EdgeInsets.only(
                                        left:
                                            SizeConfig.blockSizeHorizontal * 10,
                                        top:
                                            SizeConfig.blockSizeHorizontal * 10,
                                      ),
                                      child: SvgPicture.asset(
                                        'assets/icons/phone.svg',
                                        width: SizeConfig.blockSizeVertical * 4,
                                        height:
                                            SizeConfig.blockSizeVertical * 4,
                                        color: Colors.brown,
                                        fit: BoxFit.contain,
                                      ),
                                    ),
                                  ],
                                ),
                                Row(
                                  children: [
                                    Padding(
                                      padding: EdgeInsets.only(
                                        left:
                                            SizeConfig.blockSizeHorizontal * 12,
                                        top:
                                            SizeConfig.blockSizeHorizontal * 10,
                                      ),
                                      child: Text(
                                        'Send the email: \n ceyloncraft@gmail.com',
                                        style: TextStyle(
                                            color: Colors.brown,
                                            fontSize:
                                                SizeConfig.blockSizeVertical *
                                                    2.0,
                                            fontWeight: FontWeight.w500),
                                      ),
                                    ),
                                    Padding(
                                      padding: EdgeInsets.only(
                                        left:
                                            SizeConfig.blockSizeHorizontal * 6,
                                        top:
                                            SizeConfig.blockSizeHorizontal * 10,
                                      ),
                                      child: SvgPicture.asset(
                                        'assets/icons/email.svg',
                                        width: SizeConfig.blockSizeVertical * 4,
                                        height:
                                            SizeConfig.blockSizeVertical * 4,
                                        color: Colors.brown,
                                        fit: BoxFit.contain,
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ]),
        ),
      ),
    );
  }
}
