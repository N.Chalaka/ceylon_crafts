// ignore_for_file: camel_case_types, unnecessary_null_comparison, prefer_const_constructors, avoid_init_to_null




import 'package:ceylon_crafts/sizeConfig.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:image_picker/image_picker.dart';
import 'dart:io';

class account_page extends StatefulWidget {
  const account_page({Key? key}) : super(key: key);

  @override
  _account_pageState createState() => _account_pageState();
}

class _account_pageState extends State<account_page> with AutomaticKeepAliveClientMixin{

  
  @override
  // TODO
  bool get wantKeepAlive => true;
 
  final ImagePicker _picker = ImagePicker();
   PickedFile _pickedFile = PickedFile('') ;

  @override
  Widget build(BuildContext context) {
    super.build(context);
    SizeConfig().init(context);
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
          child: Padding(
        padding: EdgeInsets.only(
            top: SizeConfig.blockSizeVertical * 5,
            left: SizeConfig.blockSizeHorizontal * 10),
        child: Container(
          width: SizeConfig.blockSizeHorizontal * 80,
          height: SizeConfig.blockSizeVertical * 80,
          decoration: BoxDecoration(
              borderRadius:
                  BorderRadius.circular(SizeConfig.blockSizeVertical * 2),
              border: Border.all(color: Colors.brown, width: 3)),
          child: Column(
            children: [
              Stack(
                children: [
                  Padding(
                  padding: EdgeInsets.only(top: SizeConfig.blockSizeVertical * 3),
                  child: CircleAvatar(
                    backgroundImage: 
                     AssetImage('assets/shopImages/cane.jpg'),
                     
                     
                    radius: SizeConfig.blockSizeVertical*8,
                    )
                  
                ),
                Positioned(
                  bottom: -SizeConfig.blockSizeVertical*0.5,
                  right: 0,
                  child: Container(
                    width: SizeConfig.blockSizeVertical*6,
                    height: SizeConfig.blockSizeVertical*6,
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                       color: Colors.brown,
                       border: Border.all(color: Colors.white,width: 4)
                    ),
                   
                    child: IconButton(
                      onPressed: (){
                        showModalBottomSheet(context: context, 
                        builder: ((builder)=>bottomsheet()));
                      }, 
                      icon: const Icon(Icons.camera_alt),
                      color: Colors.white,
                      iconSize: SizeConfig.blockSizeVertical*3,
                      ),
                  ),
                )
                ]
              ),

              SizedBox(height: SizeConfig.blockSizeVertical,),
              Container(
                width: SizeConfig.blockSizeHorizontal * 50,
                height: SizeConfig.blockSizeVertical*8,
                child: TextButton(
                    onPressed: () {
                      Navigator.of(context).pushNamed("edit_account");
                    },
                    child: Row(
                      children: [
                        SvgPicture.asset(
                          'assets/icons/account.svg',
                          width: SizeConfig.blockSizeVertical * 3,
                          height: SizeConfig.blockSizeVertical * 3,
                          color: Colors.brown,
                          fit: BoxFit.contain,
                        ),
                        Padding(
                          padding: EdgeInsets.only(
                              left: SizeConfig.blockSizeHorizontal * 2),
                          child: Text(
                            'Account Details',
                             style: TextStyle(
                               fontSize: SizeConfig.blockSizeVertical*2,
                               fontFamily: 'Roboto'
                             ),
                          ),
                        ),
                      ],
                    )),
              ),
              Container(
                width: SizeConfig.blockSizeHorizontal * 50,
                height: SizeConfig.blockSizeVertical*8,
                child: TextButton(
                    onPressed: () {Navigator.of(context).pushNamed("order_history");},
                    child: Row(
                      children: [
                        SvgPicture.asset(
                          'assets/icons/order_history.svg',
                          width: SizeConfig.blockSizeVertical * 3,
                          height: SizeConfig.blockSizeVertical * 3,
                          color: Colors.brown,
                          fit: BoxFit.contain,
                        ),
                        Padding(
                          padding: EdgeInsets.only(
                              left: SizeConfig.blockSizeHorizontal * 2),
                          child: Text(
                            'Order History',
                            style: TextStyle(
                               fontSize: SizeConfig.blockSizeVertical*2,
                               fontFamily: 'Roboto'
                             ),),
                        ),
                      ],
                    )),
              ),
              Container(
                width: SizeConfig.blockSizeHorizontal * 50,
                height: SizeConfig.blockSizeVertical*8,
                child: TextButton(
                    onPressed: () {Navigator.of(context).pushNamed("customer_settings");},
                    child: Row(
                      children: [
                        SvgPicture.asset(
                          'assets/icons/setting.svg',
                          width: SizeConfig.blockSizeVertical * 3,
                          height: SizeConfig.blockSizeVertical * 3,
                          color: Colors.brown,
                          fit: BoxFit.contain,
                        ),
                        Padding(
                          padding: EdgeInsets.only(
                              left: SizeConfig.blockSizeHorizontal * 2),
                          child: Text(
                            'Settings',
                            style: TextStyle(
                               fontSize: SizeConfig.blockSizeVertical*2,
                               fontFamily: 'Roboto'
                             ),),
                        ),
                      ],
                    )),
              ),
              Container(
                width: SizeConfig.blockSizeHorizontal * 50,
                height: SizeConfig.blockSizeVertical*8,
                child: TextButton(
                    onPressed: () {},
                    child: Row(
                      children: [
                        SvgPicture.asset(
                          'assets/icons/signout2.svg',
                          width: SizeConfig.blockSizeVertical * 3,
                          height: SizeConfig.blockSizeVertical * 3,
                          color: Colors.brown,
                          fit: BoxFit.contain,
                        ),
                        Padding(
                          padding: EdgeInsets.only(
                              left: SizeConfig.blockSizeHorizontal * 2),
                          child: Text(
                            'Signout',
                            style: TextStyle(
                               fontSize: SizeConfig.blockSizeVertical*2,
                               fontFamily: 'Roboto'
                             ),),
                        ),
                      ],
                    )),
              ),
              Container(
                width: SizeConfig.blockSizeHorizontal * 50,
                height: SizeConfig.blockSizeVertical*8,
                child: TextButton(
                    onPressed: () {Navigator.of(context).pushNamed("help");},
                    child: Row(
                      children: [
                        SvgPicture.asset(
                          'assets/icons/help.svg',
                          width: SizeConfig.blockSizeVertical * 3,
                          height: SizeConfig.blockSizeVertical * 3,
                          color: Colors.brown,
                          fit: BoxFit.contain,
                        ),
                        Padding(
                          padding: EdgeInsets.only(
                              left: SizeConfig.blockSizeHorizontal * 2),
                          child: Text(
                            'Help',
                            style: TextStyle(
                               fontSize: SizeConfig.blockSizeVertical*2,
                               fontFamily: 'Roboto'
                             ),),
                        ),
                      ],
                    )),
              ),
            ],
          ),
        ),
      )),
    );
  }

  Widget bottomsheet(){
    return Container(
      width: SizeConfig.screenWidth,
      height: SizeConfig.blockSizeVertical*30,
      child: Column(
        children: [
          SizedBox(
            height: SizeConfig.blockSizeVertical*5,
          ),
          Text(
            'Choose an image',
            style: TextStyle(
              fontSize: SizeConfig.blockSizeVertical*3,
              color: Colors.black,
              fontWeight: FontWeight.w700
              ),
            ),
            SizedBox(
              height: SizeConfig.blockSizeVertical*5,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Padding(
                  padding: EdgeInsets.only(left: SizeConfig.safeAreaHorizontal*5,right: SizeConfig.blockSizeHorizontal*5),
                  child: TextButton.icon(
                    onPressed: (){_takePhoto(ImageSource.camera);}, 
                    icon: const Icon(Icons.camera,color: Colors.black,), 
                    label: Text(
                      'Camera',
                      style: TextStyle(color:Colors.black, fontSize: SizeConfig.blockSizeVertical*2,),
                      )
                      ),
                ),
                  Padding(
                    padding: EdgeInsets.only(left:SizeConfig.blockSizeHorizontal*5,right: SizeConfig.blockSizeHorizontal*5),
                    child: TextButton.icon(
                    onPressed: (){_takePhoto(ImageSource.gallery);}, 
                    icon: const Icon(Icons.image,color: Colors.black,), 
                    label: Text(
                      'Gallery',
                      style: TextStyle(color: Colors.black,fontSize: SizeConfig.blockSizeVertical*2,),
                      )
                      ),
                  ),
              ],
            )

        ],
      ),
    );
  }
   _takePhoto(ImageSource source) async{
    final pickedFile = await _picker.pickImage(source: source);
    setState(() {
      _pickedFile = pickedFile as PickedFile;
    });
  }
  // Container _buidAccountOption(String title,String icon,dynamic ontap) {
  //   return Container(
  //     width: SizeConfig.blockSizeHorizontal * 50,
  //     child: TextButton(
  //         onPressed: () {
  //           ontap();
  //         },
  //         child: Row(
  //           children: [
  //             SvgPicture.asset(
  //               'icon',
  //               width: SizeConfig.blockSizeVertical * 4,
  //               height: SizeConfig.blockSizeVertical * 4,
  //               color: Colors.brown,
  //               fit: BoxFit.contain,
  //             ),
  //             Padding(
  //               padding:
  //                   EdgeInsets.only(left: SizeConfig.blockSizeHorizontal * 2),
  //               child: const Text('title'),
  //             ),
  //           ],
  //         )),
  //   );
  // }

//   dynamic toEditDetails() {
//     Navigator.of(context).pushNamed("edit_account");
//   }

//   dynamic toOrderHistory() {
//     Navigator.of(context).pushNamed("order_history");
//   }

//   dynamic toCustomerSettings() {
//     Navigator.of(context).pushNamed("customer_settinngs");
//   }

//   dynamic toHelp() {
//     Navigator.of(context).pushNamed("help");
//   }
// }
}