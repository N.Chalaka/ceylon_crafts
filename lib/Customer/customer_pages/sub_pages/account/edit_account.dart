// ignore_for_file: camel_case_types, non_constant_identifier_names


import 'package:ceylon_crafts/Customer/customBoxShadow.dart';
import 'package:ceylon_crafts/sizeConfig.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class edit_account extends StatefulWidget {
  const edit_account({ Key? key }) : super(key: key);

  @override
  _edit_accountState createState() => _edit_accountState();
}

class _edit_accountState extends State<edit_account> {

  final name_controller = TextEditingController();
  final tp_controller = TextEditingController();
  final email_controller = TextEditingController();
  final address_controller = TextEditingController();
  final nic_controller = TextEditingController();
  

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        child: Column(
          children: [
            Padding(
              padding: EdgeInsets.only(bottom:SizeConfig.blockSizeVertical),
              child: Container(
                //padding: EdgeInsets.only(left:SizeConfig.blockSizeHorizontal*5),
                height: SizeConfig.blockSizeVertical*4,
                width: SizeConfig.screenWidth,
                
                alignment: Alignment.centerLeft,
                child: IconButton(onPressed:(){
                    Navigator.of(context).pop();
                  } , 
                  icon: const Icon(Icons.arrow_back_ios),color: Colors.brown,iconSize: SizeConfig.blockSizeVertical*3,
                  ),
              ),
            ),
            Expanded(
              child: SingleChildScrollView(
                child: Center(
                  child: Column(
                    children: [
                      Container(
                        width: SizeConfig.blockSizeHorizontal*90,
                        height: SizeConfig.blockSizeVertical*55,
                        decoration: BoxDecoration(
                        borderRadius:
                            BorderRadius.circular(SizeConfig.blockSizeVertical * 2),
                        border: Border.all(color: Colors.brown, width: 3)),
                        child: Column(
                          children: [
                            Container(
                              width: SizeConfig.blockSizeHorizontal * 80,
                              height: SizeConfig.blockSizeVertical * 6,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  SvgPicture.asset(
                                  'assets/icons/account.svg',
                                  width: SizeConfig.blockSizeVertical * 4,
                                  height: SizeConfig.blockSizeVertical * 4,
                                  color: Colors.brown,
                                  fit: BoxFit.contain,
                                ),
                                  Padding(
                                    padding: EdgeInsets.only(
                                        left: SizeConfig.blockSizeHorizontal * 2),
                                    child: Text('Account Details',
                                    style: TextStyle(
                                      color: Colors.brown,
                                      fontSize: SizeConfig.blockSizeVertical*2.5),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            text_form_field('Enter name', 'Name', name_controller, Icons.person),
                            text_form_field('Enter telephone number', 'Telephone number', tp_controller, Icons.phone),
                            text_form_field('Enter email', 'Email', email_controller, Icons.email),
                            text_form_field('Enter address', 'Address', address_controller, Icons.location_on),
                            text_form_field('Enter NIC', 'NIC', nic_controller, Icons.lock),
                            Row(
                             mainAxisAlignment: MainAxisAlignment.end,
                              children: [
                                Padding(
                                  padding:EdgeInsets.only(top: SizeConfig.blockSizeVertical*2,right: SizeConfig.blockSizeHorizontal*5),
                                  child: button('Change','change_account_details'),
                                )
                              ],
                            )
                          ],
                        ),
                        ),
                        SizedBox(
                        width: SizeConfig.blockSizeHorizontal*90,
                        height: SizeConfig.blockSizeVertical*3,
                        ),
                        Container(
                        width: SizeConfig.blockSizeHorizontal*90,
                        height: SizeConfig.blockSizeVertical*10,
                        decoration: BoxDecoration(
                        borderRadius:
                            BorderRadius.circular(SizeConfig.blockSizeVertical * 2),
                        border: Border.all(color: Colors.brown, width: 3)),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Padding(
                              padding: EdgeInsets.only(left: SizeConfig.blockSizeHorizontal*5),
                              child: Text('Reset Password',
                              style: TextStyle(
                                fontWeight: FontWeight.w500,
                                fontSize: SizeConfig.blockSizeVertical*2.5,
                                color: Colors.brown
                                ),
    
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.only(right: SizeConfig.blockSizeHorizontal*5),
                              child: button('Reset', 'reset_password'),
                            )
                          ],
                        ),
                        ),
                         SizedBox(
                        width: SizeConfig.blockSizeHorizontal*90,
                        height: SizeConfig.blockSizeVertical*3,
                        ),
                        Container(
                        width: SizeConfig.blockSizeHorizontal*90,
                        height: SizeConfig.blockSizeVertical*10,
                        decoration: BoxDecoration(
                        borderRadius:
                            BorderRadius.circular(SizeConfig.blockSizeVertical * 2),
                        border: Border.all(color: Colors.brown, width: 3)),
                        child:Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Padding(
                              padding: EdgeInsets.only(left: SizeConfig.blockSizeHorizontal*5),
                              child: Text('Change Bank Details',
                              style: TextStyle(
                                fontWeight: FontWeight.w500,
                                fontSize: SizeConfig.blockSizeVertical*2.5,
                                color: Colors.brown
                                ),
    
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.only(right: SizeConfig.blockSizeHorizontal*5),
                              child: button('Change', 'change_bank_details'),
                            )
                          ],
                        ),
                        ),
                         SizedBox(
                        width: SizeConfig.blockSizeHorizontal*90,
                        height: SizeConfig.blockSizeVertical*3,
                        ),
    
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
        ),
    );
  }
    Widget text_form_field(String hintText,String labelText,TextEditingController controller,IconData icon) {
    return Padding(
      padding: EdgeInsets.only(top: SizeConfig.blockSizeVertical*2),
      child: Container(
        width: SizeConfig.blockSizeHorizontal * 80,
        height: SizeConfig.blockSizeVertical * 6,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(SizeConfig.blockSizeVertical * 3),
          border: Border.all(color: Colors.grey.shade300, width: 0),
           
          boxShadow:[
            CustomBoxShadow(
              color: Colors.grey.withOpacity(0.2),
              spreadRadius: 2,
              offset: const Offset(0,1)
            )
          ]
        ),
        
        child: Padding(
          padding: EdgeInsets.all(SizeConfig.blockSizeVertical * 1),
          child: TextFormField(
            controller: controller,
            style: TextStyle(
              fontSize: SizeConfig.blockSizeVertical * 2,
            ),
            decoration: InputDecoration(
              contentPadding: EdgeInsets.all(SizeConfig.blockSizeVertical),
              
              
              floatingLabelBehavior: FloatingLabelBehavior.never,
              hintText: hintText,
              hintStyle: const TextStyle(),
      
              prefixIcon: Icon(icon,color: Colors.brown,),
              // enabledBorder:OutlineInputBorder(
              //   borderSide: BorderSide(color: Colors.grey.shade300,width: 2),
              //   borderRadius: BorderRadius.circular(SizeConfig.blockSizeVertical*3),
                
              //   ),
              //  border:OutlineInputBorder(
              //   borderSide: const BorderSide(color: Colors.brown,width: 2),
              //   borderRadius: BorderRadius.circular(SizeConfig.blockSizeVertical*3),
                
              //   ),
              labelText: labelText,
              border: const OutlineInputBorder(borderSide: BorderSide.none)
      
              // fillColor: Colors.grey.shade500,
            ),
          ),
        ),
      ),
    );
  }
  Widget button(String name, String page_name){
    return Container(
      width: SizeConfig.blockSizeHorizontal*24,
      height: SizeConfig.blockSizeVertical*5,
      
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
          shape: RoundedRectangleBorder(borderRadius:BorderRadius.circular(SizeConfig.blockSizeVertical*2) )
        ),
      onPressed: (){
         Navigator.of(context).pushNamed(page_name);
      },
      child: Text(
        name,
        style: TextStyle(fontSize: SizeConfig.blockSizeVertical*2,fontWeight: FontWeight.w700),
        ),
      ),
    );
  }
}
