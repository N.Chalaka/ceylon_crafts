// ignore_for_file: camel_case_types

import 'package:ceylon_crafts/Customer/customBoxShadow.dart';
import 'package:ceylon_crafts/sizeConfig.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class change_bank_details extends StatefulWidget {
  const change_bank_details({ Key? key }) : super(key: key);

  @override
  _change_bank_detailsState createState() => _change_bank_detailsState();
}

class _change_bank_detailsState extends State<change_bank_details> with AutomaticKeepAliveClientMixin{

  final bank_name_controller = TextEditingController();
  final branch_controller = TextEditingController();
  final acc_no_controller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    super.build(context);
    SizeConfig().init(context);
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        child: Column(
          children: [
            Padding(
              padding: EdgeInsets.only(bottom:SizeConfig.blockSizeVertical*10),
              child: Container(
                //padding: EdgeInsets.only(left:SizeConfig.blockSizeHorizontal*5),
                height: SizeConfig.blockSizeVertical*4,
                width: SizeConfig.screenWidth,
                
                alignment: Alignment.centerLeft,
                child: IconButton(onPressed:(){
                    Navigator.of(context).pop();
                  } , 
                  icon: const Icon(Icons.arrow_back_ios),color: Colors.brown,iconSize: SizeConfig.blockSizeVertical*3,
                  ),
              ),
            ),
            Expanded(
              child: SingleChildScrollView(
                child: Center(
                  child: Column(
                    children: [
                      Container(
                        width: SizeConfig.blockSizeHorizontal*90,
                        height: SizeConfig.blockSizeVertical*40,
                        decoration: BoxDecoration(
                        borderRadius:
                            BorderRadius.circular(SizeConfig.blockSizeVertical * 2),
                        border: Border.all(color: Colors.brown, width: 3)),
                        child: Column(
                          children: [
                            Container(
                              width: SizeConfig.blockSizeHorizontal * 80,
                              height: SizeConfig.blockSizeVertical * 6,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Icon(
                                  Icons.account_balance,
                                  size: SizeConfig.blockSizeVertical * 4,
                                  color: Colors.brown,
                                  
                                ),
                                  Padding(
                                    padding: EdgeInsets.only(
                                        left: SizeConfig.blockSizeHorizontal * 2),
                                    child: Text('Bank Details',
                                    style: TextStyle(
                                      color: Colors.brown,
                                      fontSize: SizeConfig.blockSizeVertical*2.5),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            text_form_field('Enter bank name', 'Name', bank_name_controller, Icons.account_balance),
                            text_form_field('Enter branch', 'Branch', branch_controller, Icons.account_balance),
                            text_form_field('Enter account number', 'Account Number', acc_no_controller, Icons.password),
                            
                            Row(
                             mainAxisAlignment: MainAxisAlignment.end,
                              children: [
                                Padding(
                                  padding:EdgeInsets.only(top: SizeConfig.blockSizeVertical*2,right: SizeConfig.blockSizeHorizontal*5),
                                  child: button('Change', (){}),
                                )
                              ],
                            )
                          ],
                        ),
                        )
                    ]
                  )
                    
                )
              )
            )
          ]
        )
      )
    );
  }
    Widget text_form_field(String hintText,String labelText,TextEditingController controller,IconData icon) {
    return Padding(
      padding: EdgeInsets.only(top: SizeConfig.blockSizeVertical*2),
      child: Container(
        width: SizeConfig.blockSizeHorizontal * 80,
        height: SizeConfig.blockSizeVertical * 6,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(SizeConfig.blockSizeVertical * 3),
          border: Border.all(color: Colors.grey.shade300, width: 0),
           
          boxShadow:[
            CustomBoxShadow(
              color: Colors.grey.withOpacity(0.2),
              spreadRadius: 2,
              offset: const Offset(0,1)
            )
          ]
        ),
        
        child: Padding(
          padding: EdgeInsets.all(SizeConfig.blockSizeVertical * 1),
          child: TextFormField(
            controller: controller,
            style: TextStyle(
              fontSize: SizeConfig.blockSizeVertical * 2,
            ),
            decoration: InputDecoration(
              contentPadding: EdgeInsets.all(SizeConfig.blockSizeVertical),
              
              
              floatingLabelBehavior: FloatingLabelBehavior.never,
              hintText: hintText,
              hintStyle: const TextStyle(),
      
              prefixIcon: Icon(icon,color: Colors.brown,),
              // enabledBorder:OutlineInputBorder(
              //   borderSide: BorderSide(color: Colors.grey.shade300,width: 2),
              //   borderRadius: BorderRadius.circular(SizeConfig.blockSizeVertical*3),
                
              //   ),
              //  border:OutlineInputBorder(
              //   borderSide: const BorderSide(color: Colors.brown,width: 2),
              //   borderRadius: BorderRadius.circular(SizeConfig.blockSizeVertical*3),
                
              //   ),
              labelText: labelText,
              border: const OutlineInputBorder(borderSide: BorderSide.none)
      
              // fillColor: Colors.grey.shade500,
            ),
          ),
        ),
      ),
    );
  }
  Widget button(String name,void function){
    return Container(
      width: SizeConfig.blockSizeHorizontal*24,
      height: SizeConfig.blockSizeVertical*5,
      
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
          shape: RoundedRectangleBorder(borderRadius:BorderRadius.circular(SizeConfig.blockSizeVertical*2) )
        ),
      onPressed: (){
        function;
      },
      child: Text(
        name,
        style: TextStyle(fontSize: SizeConfig.blockSizeVertical*2,fontWeight: FontWeight.w700),
        ),
      ),
    );
  }

  @override
  // TODO: implement wantKeepAlive
  bool get wantKeepAlive => true;
}