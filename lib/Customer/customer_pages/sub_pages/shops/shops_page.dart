import 'package:ceylon_crafts/Customer/customer_pages/sub_pages/shops/shopBuilder.dart';
import 'package:ceylon_crafts/Customer/customer_pages/sub_pages/shops/shopslist.dart';
import 'package:ceylon_crafts/searchWidget.dart';
import 'package:ceylon_crafts/sizeConfig.dart';
import 'package:flutter/material.dart';
import 'shops.dart';

class shops_page extends StatefulWidget {
  const shops_page({ Key? key }) : super(key: key);

  @override
  _shops_pageState createState() => _shops_pageState();
}

class _shops_pageState extends State<shops_page> with AutomaticKeepAliveClientMixin{
  String query = '';
  
  late List<Shops> shops;

  @override
  void initState(){
    super.initState();
    shops = shop_list.cast<Shops>();
  }

  
  @override
  Widget build(BuildContext context) {
    super.build(context);
    SizeConfig().init(context);
    return GestureDetector(
      onTap: (){
        FocusScopeNode currentFocus = FocusScope.of(context);
        if(!currentFocus.hasPrimaryFocus){
          currentFocus.unfocus();
          
        }
      },
      child: Scaffold(
        backgroundColor: Colors.white,
        extendBodyBehindAppBar: true,
        appBar: PreferredSize(
            preferredSize: Size.fromHeight(SizeConfig.blockSizeVertical*12),
            child: Column(children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Container(
                    margin: EdgeInsets.only(top:SizeConfig.safeAreaVertical+SizeConfig.blockSizeVertical,left: SizeConfig.blockSizeHorizontal*2),
                   // height: SizeConfig.blockSizeVertical * 9,
                    width: 3 * SizeConfig.screenWidth / 4,
                    child: buildSearch(),
                  ),
                  Container(
                    margin: EdgeInsets.only(top:SizeConfig.safeAreaVertical+SizeConfig.blockSizeVertical),
                    child: IconButton(
                      icon: const Icon(Icons.notifications,color: Colors.brown,),
                      iconSize: SizeConfig.blockSizeVertical*5,
                      onPressed: (){}
                    ,),
                  )
                ],
              ),
            ]),
          ),
        
        body: SafeArea(
            child: ListView.builder(
                itemCount: shops.length,
                itemBuilder: (context, index) {
                  final shop = shops[index];
                  return shopBuilder(shop);
                }
                
                )
                ),
      
      )
    );



    
  }
  Widget buildSearch() {
  return searchWidget(
      text: query,
       onChanged: searchShop,
        hintText: 'Search shops here..',
        bordercolor:Colors.brown,
        );
}

void searchShop(String query){
  final shoplist = shop_list.where((shop){
    final  nameLower = shop.shopName.toLowerCase();
    final  locationLower = shop.location.toLowerCase();
    final  searchLower = query.toLowerCase();

    return  nameLower.contains(searchLower)||
      locationLower.contains(searchLower);
  }).toList();

  setState(() {
    this.query = query;
    shops = shoplist.cast<Shops>();
  });

}

  @override
  
  bool get wantKeepAlive => true;

}

