// ignore_for_file: file_names

import 'package:flutter/material.dart';

import 'shops.dart';

List<Shops> shop_list = [
    Shops(
        shopName: 'Sri Lankan Wooden Masks and woodcrafts',
        createdDate: '2 months ago',
        location: 'Kandy',
        rating: 3,
        image: Image.asset('assets/shopImages/wooden_masks.png')),
    Shops(
        shopName: 'Malka Wool Creations',
        createdDate: '2 weeks ago',
        location: 'Kaluthara',
        rating: 3.5,
        image: Image.asset('assets/shopImages/wool_design.png')),
    Shops(
        shopName: 'Dharmadasa woodcrafts',
        createdDate: '3 weeks ago',
        location: 'Kandy',
        rating: 4,
        image: Image.asset('assets/shopImages/woodcrafts.png')),
    Shops(
        shopName: 'Pottery Shop',
        createdDate: '2 weeks ago',
        location: 'Kegalle',
        rating: 4,
        image: Image.asset('assets/shopImages/clay_item.png')),
    Shops(
        shopName: 'Masks collection',
        createdDate: '1 week ago',
        location: 'Colombo',
        rating: 3.75,
        image: Image.asset('assets/shopImages/mask.png')),
    Shops(
        shopName: 'Paintings',
        createdDate: '1 day ago',
        location: 'Gampaha',
        rating: 3.5,
        image: Image.asset('assets/shopImages/paintings.png')),
    Shops(
        shopName: 'Cotton Pillows',
        createdDate: '1 week ago',
        location: 'Kandy',
        rating: 3,
        image: Image.asset('assets/shopImages/pillows.png')),
    Shops(
        shopName: 'Cane Crafts',
        createdDate: '2 weeks ago',
        location: 'Battaramulla',
        rating: 4,
        image: Image.asset('assets/shopImages/cane.jpg')),
    Shops(
        shopName: 'Bathik Shop',
        createdDate: '1 week ago',
        location: 'Kegalle',
        rating: 3.75,
        image: Image.asset('assets/shopImages/bathik.jpg')),
  ];
