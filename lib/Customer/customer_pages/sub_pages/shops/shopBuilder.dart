// ignore_for_file: file_names


import 'package:ceylon_crafts/sizeConfig.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'shops.dart';


Widget shopBuilder(Shops shop) {
  return Padding(
    padding: EdgeInsets.fromLTRB(SizeConfig.blockSizeHorizontal, SizeConfig.blockSizeVertical, SizeConfig.blockSizeHorizontal, SizeConfig.blockSizeVertical*0.5),
    child: Card(
      elevation: 0,
       shape: RoundedRectangleBorder(
           side:  BorderSide(color: Colors.grey.withOpacity(0.5),width: 2),
            borderRadius: BorderRadius.circular(SizeConfig.blockSizeVertical*2)
          ),
          
      //height: SizeConfig.blockSizeVertical * 20,
     
    /* decoration: BoxDecoration(
        color: Colors.transparent,
        borderRadius:BorderRadius.circular(SizeConfig.blockSizeVertical*2),
      /*  boxShadow: const [BoxShadow(
          color: Colors.white,
          spreadRadius: 1,
        )]*/
         ),*/
      child: InkWell(
        splashColor:Colors.grey.shade300,
            onTap: (){},
        child: Container(
          width: SizeConfig.blockSizeHorizontal * 90,
       // color: Colors.white,
        
         // color: Colors.white,
          child: Row(
           // mainAxisAlignment: MainAxisAlignment.start,
            //crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Padding(
                padding: EdgeInsets.only(left: SizeConfig.blockSizeHorizontal * 2),
                child: Column(
                 mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Container(
                      height: SizeConfig.blockSizeHorizontal * 18,
                      width: SizeConfig.blockSizeHorizontal * 18,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(
                              SizeConfig.blockSizeVertical ),
                              
                              ),
                      child: shop.image,
                    )
                  ],
                ),
              ),
              Column(
                // mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: EdgeInsets.only(
                        top: SizeConfig.blockSizeVertical * 1.5,
                        left: SizeConfig.blockSizeHorizontal * 4),
                    child: Container(
                      width: SizeConfig.blockSizeHorizontal * 60,
                      child: Wrap(
                        children: 
                          [Text(
                            shop.shopName,
                            style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.w400,
                                fontSize: SizeConfig.blockSizeVertical * 2),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                      top: SizeConfig.blockSizeVertical * 1,
                      left: SizeConfig.blockSizeHorizontal * 4,
                    ),
                    child: Container(
                      width: SizeConfig.blockSizeHorizontal * 60,
                      child: Text(
                        shop.createdDate,
                        style: TextStyle(
                          color: Colors.grey,
                          fontSize: SizeConfig.blockSizeVertical * 1.5,
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                        top: SizeConfig.blockSizeVertical * 1,
                        left: SizeConfig.blockSizeHorizontal * 4,
                        ),
                    child: Container(
                      height: SizeConfig.blockSizeVertical * 3,
                     // width: SizeConfig.blockSizeHorizontal * 20,
                      decoration: BoxDecoration(
                          borderRadius:
                              BorderRadius.circular(SizeConfig.blockSizeVertical),
                          color: Colors.brown),
                      child: Padding(
                        padding:
                            EdgeInsets.fromLTRB(SizeConfig.blockSizeHorizontal*2, SizeConfig.blockSizeVertical*0.1, SizeConfig.blockSizeHorizontal*2, SizeConfig.blockSizeVertical*0.1),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Text(
                              shop.location,
                              
                              style: TextStyle(
                                color: Colors.white,
                                
                                fontSize: SizeConfig.blockSizeVertical * 1.5,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                        top: SizeConfig.blockSizeVertical * 1,
                        bottom: SizeConfig.blockSizeVertical * 1,
                        left: SizeConfig.blockSizeHorizontal * 4),
                    child: Container(
                        width: SizeConfig.blockSizeHorizontal * 60,
                        child: RatingBarIndicator(
                          itemBuilder: (context, index) {
                            return const Icon(
                              Icons.star,
                              color: Colors.amber,
                            );
                          },
                          rating: shop.rating,
                          unratedColor: Colors.amber.withOpacity(0.2),
                          itemSize: SizeConfig.blockSizeVertical * 2.5,
                        )),
                  ),
                ],
              ),
              Column(
                
               //crossAxisAlignment: CrossAxisAlignment.start,
               // mainAxisSize: MainAxisSize.min,
                children: [
                  Padding(
                    padding: EdgeInsets.only(left: SizeConfig.blockSizeHorizontal*0.1,),
                    child: Container(
                      width: SizeConfig.blockSizeHorizontal*5,
                     // height: SizeConfig.blockSizeVertical*20,
                      alignment: Alignment.topCenter,
                      child: IconButton(
                        icon: const Icon(Icons.more_horiz),
                        onPressed: () {},
                        iconSize: SizeConfig.blockSizeHorizontal * 4,
                        color: Colors.brown,
                      ),
                    ),
                  )
                ],
              )
            ],
          ),
          //shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(SizeConfig.blockSizeVertical*2),),
        ),
      ),
    ),
  );
}
