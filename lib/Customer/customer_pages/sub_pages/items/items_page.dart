// ignore_for_file: camel_case_types

import 'package:ceylon_crafts/Customer/customBoxShadow.dart';
import 'package:ceylon_crafts/searchWidget.dart';
import 'package:ceylon_crafts/sizeConfig.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import 'Item.dart';
import 'item_details.dart';
import 'itembuilder.dart';
import 'itemlist.dart';

class items_page extends StatefulWidget {
  const items_page({ Key? key }) : super(key: key);

  @override
  _items_pageState createState() => _items_pageState();
}

class _items_pageState extends State<items_page> with SingleTickerProviderStateMixin{
  String query = '';
  late List<Item> items;
   late TabController _tabController;


  @override
  void initState(){
    super.initState();
    items = list;
    _tabController = TabController(length: 6, vsync: this);
    _tabController.addListener(_handleTabSelection);
  }
  void _handleTabSelection() {
    setState(() {
      
    });
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }
  

  void deleteItem(item,itemlist){
    //itemlist.remove(item);
    setState(() {
      itemlist.remove(item);
    });
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return GestureDetector(
      onTap: (){
        FocusScopeNode currentFocus = FocusScope.of(context);
        if(!currentFocus.hasPrimaryFocus){
          currentFocus.unfocus();
          
        }
      },
      child: DefaultTabController(
        length: 6,
        initialIndex: 1,
        child: Scaffold(
          backgroundColor: Colors.white,
          extendBodyBehindAppBar: true,
          appBar:PreferredSize(
              preferredSize: Size.fromHeight(SizeConfig.blockSizeVertical * 20),
              child: Padding(
                padding: EdgeInsets.only(top: SizeConfig.safeAreaVertical),
                child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Container(
                          margin: EdgeInsets.only(left: SizeConfig.screenWidth / 16,),
                         // height: SizeConfig.blockSizeVertical * 8,
                          width: 3 * SizeConfig.screenWidth / 4,
                          color: Colors.white,
                          child: buildSearch(),
                        ),
                        /*notifications(SizeConfig.blockSizeVertical * 3,
                            SizeConfig.blockSizeVertical * 3)*/
                          Container(
                            //height: SizeConfig.blockSizeVertical * 8,
                          width: SizeConfig.screenWidth / 8,
                          color: Colors.white,
                            child: IconButton(
                              onPressed: (){},
                               icon: Icon(Icons.notifications,color: Colors.brown,size: SizeConfig.blockSizeVertical*3.5,),
                               ),
                          ),
                      ],
                    ),
                    Container(
                      height: SizeConfig.blockSizeVertical * 10,
                      width: SizeConfig.screenWidth,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          border: Border.all(color: Colors.white),
                          borderRadius: BorderRadius.only(
                            bottomLeft:
                                Radius.circular(SizeConfig.blockSizeVertical * 2),
                            bottomRight:
                                Radius.circular(SizeConfig.blockSizeVertical * 2),
                          ),
                          boxShadow: [
                            CustomBoxShadow(
                                color: Colors.grey.withOpacity(0.5),
                                blurRadius: 3,
                                spreadRadius: 1,
                                offset: const Offset(0, 5))
                          ]),
                      child: TabBar(
                        controller: _tabController,
                        
                        isScrollable: true,
                        indicatorColor: Colors.transparent,
                        labelPadding: EdgeInsets.only(
                            left: SizeConfig.blockSizeHorizontal * 3,
                            right: SizeConfig.blockSizeHorizontal * 3),
                        labelColor: Colors.green,
                        unselectedLabelColor: Colors.brown,
                        labelStyle: TextStyle(
                          fontSize: SizeConfig.blockSizeVertical * 1.5,
                        ),
      
                        // padding: const EdgeInsets.symmetric(horizontal: 0),
                        tabs: [
                          Tab(
                            height: SizeConfig.blockSizeVertical * 10,
                            iconMargin:const  EdgeInsets.only(bottom: 0),
                            icon: SvgPicture.asset(
                              'assets/icons/masks.svg',
                              width: SizeConfig.blockSizeVertical * 4,
                              height: SizeConfig.blockSizeVertical * 4,
                              fit: BoxFit.cover,
                              color: _tabController.index == 0
                                  ? Colors.green
                                  : Colors.brown,
                            ),
                            text: 'Masks',
                          ),
                          Tab(
                            height: SizeConfig.blockSizeVertical * 10,
                            iconMargin: EdgeInsets.only(bottom: 0),
                            icon: SvgPicture.asset(
                              'assets/icons/pottery.svg',
                              width: SizeConfig.blockSizeVertical * 4,
                              height: SizeConfig.blockSizeVertical * 4,
                              fit: BoxFit.cover,
                              color: _tabController.index == 1
                                  ? Colors.green
                                  : Colors.brown,
                            ),
                            text: 'Pottery',
                          ),
                          Tab(
                            height: SizeConfig.blockSizeVertical * 10,
                            iconMargin: EdgeInsets.only(bottom: 0),
                            icon: SvgPicture.asset(
                              'assets/icons/wooden.svg',
                              width: SizeConfig.blockSizeVertical * 4,
                              height: SizeConfig.blockSizeVertical * 4,
                              fit: BoxFit.cover,
                              color: _tabController.index == 2
                                  ? Colors.green
                                  : Colors.brown,
                            ),
                            text: 'WoodCrafts',
                          ),
                          Tab(
                             height: SizeConfig.blockSizeVertical * 10,
                             iconMargin: EdgeInsets.only(bottom: 0),
                            icon: SvgPicture.asset(
                              'assets/icons/paintings.svg',
                              width: SizeConfig.blockSizeVertical * 4,
                              height: SizeConfig.blockSizeVertical * 4,
                              fit: BoxFit.cover,
                              color: _tabController.index == 3
                                  ? Colors.green
                                  : Colors.brown,
                            ),
                            text: 'Paintings',
                          ),
                          Tab(
                            height: SizeConfig.blockSizeVertical * 10,
                            iconMargin: EdgeInsets.only(bottom: 0),
                            icon: SvgPicture.asset(
                              'assets/icons/wool.svg',
                              width: SizeConfig.blockSizeVertical * 4,
                              height: SizeConfig.blockSizeVertical * 4,
                              fit: BoxFit.cover,
                              color: _tabController.index == 4
                                  ? Colors.green
                                  : Colors.brown,
                            ),
                            text: 'Wool',
                          ),
                          Tab(
                            height: SizeConfig.blockSizeVertical * 10,
                            iconMargin: EdgeInsets.only(bottom: 0),
                            icon: SvgPicture.asset(
                              'assets/icons/leather.svg',
                              width: SizeConfig.blockSizeVertical * 4,
                              height: SizeConfig.blockSizeVertical * 4,
                              fit: BoxFit.cover,
                              color: _tabController.index == 5
                                  ? Colors.green
                                  : Colors.brown,
                            ),
                            text: 'Leather',
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          body: SafeArea(
            child: ListView.builder(
               itemCount: items.length,
               itemBuilder: (context, index) {
                 final item = items[index];
                 return item_builder(item: item,ontap: ()=>displayDetails(item),);
               }
               
               ),
               
            ),
            // floatingActionButton: Container(
            //   width: SizeConfig.blockSizeVertical*10,
            //   height: SizeConfig.blockSizeVertical*10,
            //   child: FittedBox(
            //     child: FloatingActionButton(
            //       backgroundColor: Colors.brown,
                  
            //       child: Column(
            //         children: [
            //           Icon(Icons.add,color: Colors.white,size:35,),
            //           Text(
            //             'ADD',
            //             style: TextStyle(
            //               color: Colors.white,
            //               fontSize: 10
            //             ),
            //             )
            //         ],),
            //       onPressed: (){
                    
            //       },
            //       ),
            //   ),
            // ),
              
        ),
      ),
    );
  }

  Widget buildSearch() {
  return searchWidget(
      text: query,
       onChanged: searchItems,
        hintText: 'Search items here..',
        bordercolor: Colors.grey,
        );
}

void searchItems(String query){
  final itemslist = list.where((item){
    final  nameLower = item.itemName.toLowerCase();
    final  locationLower = item.location.toLowerCase();
    final  searchLower = query.toLowerCase();

    return  nameLower.contains(searchLower)||
      locationLower.contains(searchLower);
  }).toList();

  setState(() {
    this.query = query;
    items = itemslist;
  });

}

  Widget itemDetails(Item item){
    return Container(
     width: SizeConfig.blockSizeHorizontal*80,
      height: SizeConfig.blockSizeVertical*80,
      child: SingleChildScrollView(
        child: Column(
          children: [
            SizedBox(
              height: SizeConfig.blockSizeVertical*5,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  IconButton(
                    onPressed: (){
                      Navigator.of(context).pop(context);
                    },
                    icon: Padding(
                      padding: EdgeInsets.only(top: SizeConfig.blockSizeVertical*0.5,right: SizeConfig.blockSizeHorizontal*2),
                      child: const Icon(Icons.close),
                    ),color: Colors.grey,iconSize: SizeConfig.blockSizeVertical*4,),
                ],
              ),
            ),
            item_details(item: item),
          ],
        ),
      )
     );
    
    
   
    
  }
  void  displayDetails(Item item){
    showModalBottomSheet(context: context,
    builder: ((builder)=>itemDetails(item)),
    isScrollControlled: true,
    backgroundColor: Colors.white,
    shape: RoundedRectangleBorder(borderRadius: BorderRadius.only(topLeft:Radius.circular(SizeConfig.blockSizeVertical*3),topRight: Radius.circular(SizeConfig.blockSizeVertical*3)))
    );
    // setState(() {
    //    showModalBottomSheet(context: context,builder: ((builder)=>itemDetails()));
    // });
   
  }
}


 
