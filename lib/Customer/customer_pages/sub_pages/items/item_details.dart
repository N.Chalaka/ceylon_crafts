// ignore_for_file: camel_case_types


import 'package:ceylon_crafts/sizeConfig.dart';
import 'package:flutter/material.dart';

import 'Item.dart';

class item_details extends StatefulWidget {
  final Item item;
  const item_details({ Key? key, required this.item}) : super(key: key);

  @override
  _item_detailsState createState() => _item_detailsState();
}

class _item_detailsState extends State<item_details> {
  List<String> imageslist= []; 

  int selectedimage = 0;
  int quantity = 1;
  

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Container(
      child: SingleChildScrollView(
        child: Column(
          children: [
            Stack(
              children:[ 
              Row(
                children: [
                  Padding(
                    padding: EdgeInsets.only(left: SizeConfig.blockSizeHorizontal*8,right: SizeConfig.blockSizeHorizontal*5),
                    child: Container(
                    padding: EdgeInsets.fromLTRB(SizeConfig.blockSizeHorizontal*2, SizeConfig.blockSizeVertical, SizeConfig.blockSizeHorizontal*2, SizeConfig.blockSizeVertical),
                    width: SizeConfig.blockSizeVertical*20,
                     height:SizeConfig.blockSizeVertical*20 ,
                     decoration: BoxDecoration(
                       
                       borderRadius: BorderRadius.circular(SizeConfig.blockSizeHorizontal*2),
                       border: Border.all(width: 2,color: Colors.brown)
                       ),
                     child: GestureDetector(
                      onTap: () {
                        
                      },
                      child: Image.asset(
                       widget.item.images[selectedimage],
                       // height:SizeConfig.blockSizeVertical*40 ,
                        
                        fit: BoxFit.cover,
                        ),
                    ),
                ),
                  ),
                 Padding(
              padding: EdgeInsets.only(left: SizeConfig.blockSizeHorizontal*2,right: SizeConfig.blockSizeHorizontal*2,top: SizeConfig.blockSizeVertical*2,bottom: SizeConfig.blockSizeVertical*2),
              child: Container(
                color: Colors.white,
               
                child: Wrap(
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Container(
                          width: SizeConfig.blockSizeHorizontal*40,
                          child: Text(
                            widget.item.itemName,
                            style:TextStyle(
                              fontSize: SizeConfig.blockSizeVertical*2.5,
                              color: Colors.grey[900],
                              fontFamily: 'Roboto',
                              fontWeight: FontWeight.w400),
                            ),
                        ),
                        SizedBox(
                          width: SizeConfig.blockSizeHorizontal*40,
                          height: SizeConfig.blockSizeVertical*3,
                        ),
                          Container(
                             width: SizeConfig.blockSizeHorizontal*40,
                          child: Text(
                            'Rs.  ${widget.item.price}',
                            style: TextStyle(
                            fontSize: SizeConfig.blockSizeVertical*2.5,
                            color: Colors.green,
                            fontWeight: FontWeight.w500
                        ),
                            ),
                        )
                      ],
                    )
                    ]),
              ),
            ),
                ],
              ),
              Positioned(
                top: SizeConfig.safeAreaVertical,
                left: SizeConfig.blockSizeHorizontal,
                child: Padding(
                  padding: EdgeInsets.only(top: SizeConfig.blockSizeVertical),
                  child: IconButton(
                  icon:Icon(Icons.arrow_back_ios,color: Colors.white,size: SizeConfig.blockSizeVertical*4,),
                   onPressed: (){
                   Navigator.of(context).pop();
                          }),
                ),
              ),
              ]
            ),
            Padding(
              padding: EdgeInsets.fromLTRB(SizeConfig.blockSizeHorizontal*5, SizeConfig.blockSizeVertical, SizeConfig.blockSizeHorizontal*5, SizeConfig.blockSizeVertical),
              child: const Divider(
                height: 2,
                
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: SizeConfig.blockSizeVertical*2),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  ...List.generate(widget.item.images.length, (index) => buildsmallpreview(index))
                ],
                ),
            ),
           Padding(
              padding: EdgeInsets.fromLTRB(SizeConfig.blockSizeHorizontal*5, SizeConfig.blockSizeVertical, SizeConfig.blockSizeHorizontal*5, SizeConfig.blockSizeVertical),
              child: const Divider(
                height: 2,
                
              ),
            ),
            Padding(
              padding: EdgeInsets.only(left: SizeConfig.blockSizeHorizontal*4,right: SizeConfig.blockSizeHorizontal*4,top: SizeConfig.blockSizeVertical*2,bottom: SizeConfig.blockSizeVertical*2),
              child: Container(
                width: SizeConfig.blockSizeHorizontal*80,
                color: Colors.white,
                child: Wrap(
                  children: [
                    Text(
                      widget.item.description,
                      style: TextStyle(
                         fontSize: SizeConfig.blockSizeVertical*2,
                        fontFamily: 'Roboto',
                        
                        color: Colors.grey[800],
                        //fontWeight: FontWeight.w200
                      ) 
                    ) 
                    ]),
              ),
            ),
            Padding(
              padding: EdgeInsets.fromLTRB(SizeConfig.blockSizeHorizontal*5, SizeConfig.blockSizeVertical, SizeConfig.blockSizeHorizontal*5, SizeConfig.blockSizeVertical),
              child: const Divider(
                height: 2,
                
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: SizeConfig.blockSizeVertical,left: SizeConfig.blockSizeHorizontal*6,right: SizeConfig.blockSizeHorizontal*6),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                   
                    width: SizeConfig.blockSizeHorizontal*30,
                    height: SizeConfig.blockSizeVertical*8,
                    child:Align(
                      alignment: Alignment.center,
                      child: Text(
                        'Quantity',
                        
                        style: TextStyle(
                              fontSize: SizeConfig.blockSizeVertical*2,
                              color: Colors.grey[800],
                              //fontWeight: FontWeight.w500
                              
                          ),
                        ),
                    )
                  ),
                  Container(
                    
                    width: SizeConfig.blockSizeHorizontal*30,
                    height: SizeConfig.blockSizeVertical*8,
                    child:Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Container(
                           width: SizeConfig.blockSizeHorizontal*7,
                          height: SizeConfig.blockSizeHorizontal*7,
                          child: FloatingActionButton(
                            onPressed: (){
                              setState(() {
                                if(quantity == 0){
                                  quantity = 0;
                                }
                                else{
                                quantity--;
                                }
                              });
                            },
                            
                             child:Center(
                               child: Text('-',
                               style: TextStyle(fontSize:SizeConfig.blockSizeHorizontal*6, 
                               ),
                               ),
                             )
                            
                             ),
                        ),
                        Text(
                          '$quantity',
                          style: TextStyle(fontSize: SizeConfig.blockSizeVertical*2),
                          ),
                        Container(
                          width: SizeConfig.blockSizeHorizontal*7,
                          height: SizeConfig.blockSizeHorizontal*7,
                          child: FloatingActionButton(
                            
                            onPressed: (){
                              setState(() {
                                if(quantity<widget.item.stock){
                                quantity++;
                                }
                                // else{
                                //   quantity = stock;
                                // }
                                  
                                
                              });
                            },
                            child:Center(
                              child: Text('+',
                              
                               style: TextStyle(fontSize:SizeConfig.blockSizeHorizontal*6,
                               )
                               ),
                            ),
                        ),
                        )
                      ],
                      )
                  ),
                  
                ],
              ),
            ),
            Padding(
              padding: EdgeInsets.fromLTRB(SizeConfig.blockSizeHorizontal*5, SizeConfig.blockSizeVertical*2, SizeConfig.blockSizeHorizontal*5, SizeConfig.blockSizeVertical),
              child:const Divider(
                height: 2,
              ),
            ),
            SizedBox(
              height: SizeConfig.blockSizeVertical*3,
              width: SizeConfig.blockSizeHorizontal*80,
            ),
            Row(
              mainAxisAlignment:MainAxisAlignment.spaceEvenly,
              children: [
                Container(
                  width: SizeConfig.blockSizeHorizontal*35,
                  height: SizeConfig.blockSizeVertical*6,
                  child: ElevatedButton(
                    onPressed: (){},
                    style: ButtonStyle(
                      backgroundColor:MaterialStateProperty.all(Colors.green) 
                    ),
                    child: Text(
                      'Buy now',
                      style: TextStyle(
                        fontSize: SizeConfig.blockSizeVertical*2,
                        color: Colors.white
                        ),
                    )
                  ,),
                ),
                Container(
                  width: SizeConfig.blockSizeHorizontal*35,
                  height: SizeConfig.blockSizeVertical*6,
                  child: ElevatedButton(
                    onPressed: (){},
                    style: ButtonStyle(
                      backgroundColor:MaterialStateProperty.all(Colors.green) 
                    ),
                    child: Text(
                      'Add to Cart',
                      style: TextStyle(
                        fontSize: SizeConfig.blockSizeVertical*2,
                        color: Colors.white
                        ),
                    )
                  ,),
                ),
              ],
            )
          ],
        ),
      )
    );
  }
  GestureDetector buildsmallpreview(int index){
    return GestureDetector(
      onTap: (){
        setState(() {
          selectedimage = index;
        });
      },
      child: Container(
        margin: EdgeInsets.only(left: SizeConfig.blockSizeHorizontal*2,right: SizeConfig.blockSizeHorizontal*2),
        padding: EdgeInsets.fromLTRB(SizeConfig.blockSizeHorizontal*2, SizeConfig.blockSizeVertical, SizeConfig.blockSizeHorizontal*2, SizeConfig.blockSizeVertical),
        width: SizeConfig.blockSizeHorizontal*12,
        height: SizeConfig.blockSizeHorizontal*12,
        decoration: BoxDecoration(
          color: Colors.grey[100],
          borderRadius: BorderRadius.circular(SizeConfig.blockSizeHorizontal*2),
          border: Border.all(width: 2, color: selectedimage == index ? Colors.brown : Colors.transparent)
          
          ), 
        child: Image.asset(
          widget.item.images[index],
          fit: BoxFit.cover,
          ), 
          
        
      ),
    );
  }
  Container img(String img){
    return Container(
      height:SizeConfig.blockSizeVertical*50 ,
              child: GestureDetector(
                onTap: () {
                  
                },
                child: Image.asset(
                  img,
                 // height:SizeConfig.blockSizeVertical*40 ,
                  width: SizeConfig.screenWidth,
                  fit: BoxFit.cover,
                  ),
              ),
    );
  }
}
