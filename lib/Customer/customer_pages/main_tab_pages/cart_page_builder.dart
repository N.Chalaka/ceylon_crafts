// ignore_for_file: camel_case_types


import 'package:ceylon_crafts/Customer/customer_pages/sub_pages/cart/cart_page.dart';
import 'package:flutter/material.dart';

class cart_page_builder extends StatefulWidget {
  const cart_page_builder({ Key? key }) : super(key: key);

  @override
  _cart_page_builderState createState() => _cart_page_builderState();
}

class _cart_page_builderState extends State<cart_page_builder> with AutomaticKeepAliveClientMixin{
  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Navigator(
      initialRoute: 'cart',
      onGenerateRoute: (RouteSettings settings){
        switch(settings.name){
          case 'cart':
            return MaterialPageRoute(builder: (context)=>const cart_page(),settings: settings);
          
          // case 'edit_account':
          // return MaterialPageRoute(builder: (context)=>const edit_account(),settings: settings);

          // case 'order_history':
          // return MaterialPageRoute(builder: (context)=>const order_history(),settings: settings);
         

          default:
            throw Exception('invalid route');
        }     
         },
    );
  }

  @override
  
  bool get wantKeepAlive => true;
}