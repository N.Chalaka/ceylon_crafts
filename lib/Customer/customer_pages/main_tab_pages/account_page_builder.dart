// ignore_for_file: camel_case_types

import 'package:ceylon_crafts/Customer/customer_pages/sub_pages/account/account_details/account_details_update.dart';
import 'package:ceylon_crafts/Customer/customer_pages/sub_pages/account/account_details/change_bank_details.dart';
import 'package:ceylon_crafts/Customer/customer_pages/sub_pages/account/account_details/reset_password.dart';
import 'package:ceylon_crafts/Customer/customer_pages/sub_pages/account/account_page.dart';
import 'package:ceylon_crafts/Customer/customer_pages/sub_pages/account/customer_settings.dart';
import 'package:ceylon_crafts/Customer/customer_pages/sub_pages/account/edit_account.dart';
import 'package:ceylon_crafts/Customer/customer_pages/sub_pages/account/help.dart';
import 'package:ceylon_crafts/Customer/customer_pages/sub_pages/account/order history/order_history.dart';
import 'package:flutter/material.dart';

class account_page_builder extends StatefulWidget {
  const account_page_builder({Key? key}) : super(key: key);

  @override
  _account_page_builderState createState() => _account_page_builderState();
}

class _account_page_builderState extends State<account_page_builder>
    with AutomaticKeepAliveClientMixin {
  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Navigator(
      initialRoute: 'account',
      onGenerateRoute: (RouteSettings settings) {
        switch (settings.name) {
          case 'account':
            return MaterialPageRoute(
                builder: (context) => const account_page(), settings: settings);

          case 'edit_account':
            return MaterialPageRoute(
                builder: (context) => const edit_account(), settings: settings);

          case 'change_account_details':
            return MaterialPageRoute(
                builder: (context) => const account_details_update(),
                settings: settings);

          case 'change_bank_details':
            return MaterialPageRoute(
                builder: (context) => const change_bank_details(),
                settings: settings);

          case 'reset_password':
            return MaterialPageRoute(
                builder: (context) => const reset_password(),
                settings: settings);

          case 'order_history':
            return MaterialPageRoute(
                builder: (context) => const order_history(),
                settings: settings);

          case 'customer_settings':
            return MaterialPageRoute(
                builder: (context) => const customer_settings(),
                settings: settings);

          case 'help':
            return MaterialPageRoute(
                builder: (context) => const help(), settings: settings);

          default:
            throw Exception('invalid route');
        }
      },
    );
  }

  @override
  bool get wantKeepAlive => true;
}
