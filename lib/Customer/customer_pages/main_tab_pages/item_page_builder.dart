// ignore_for_file: camel_case_types


import 'package:ceylon_crafts/Customer/customer_pages/sub_pages/items/items_page.dart';
import 'package:flutter/material.dart';

class item_page_builder extends StatefulWidget {
  const item_page_builder({ Key? key }) : super(key: key);

  @override
  _item_page_builderState createState() => _item_page_builderState();
}

class _item_page_builderState extends State<item_page_builder> with AutomaticKeepAliveClientMixin {
  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Navigator(
      initialRoute: 'items',
      onGenerateRoute: (RouteSettings settings){
        switch(settings.name){
          case 'items':
            return MaterialPageRoute(builder: (context)=>const items_page(),settings: settings);
          
          // case 'edit_account':
          // return MaterialPageRoute(builder: (context)=>const edit_account(),settings: settings);

          // case 'order_history':
          // return MaterialPageRoute(builder: (context)=>const order_history(),settings: settings);
         

          default:
            throw Exception('invalid route');
        }     
         },
    );
  }

  @override
 
  bool get wantKeepAlive => true;
}