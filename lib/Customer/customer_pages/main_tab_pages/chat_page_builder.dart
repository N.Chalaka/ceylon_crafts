// ignore_for_file: camel_case_types


import 'package:ceylon_crafts/Customer/customer_pages/sub_pages/chat/chat_page.dart';
import 'package:flutter/material.dart';

class chat_page_builder extends StatefulWidget {
  const chat_page_builder({ Key? key }) : super(key: key);

  @override
  _chat_page_builderState createState() => _chat_page_builderState();
}

class _chat_page_builderState extends State<chat_page_builder> with AutomaticKeepAliveClientMixin {
  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Navigator(
      initialRoute: 'chat',
      onGenerateRoute: (RouteSettings settings){
        switch(settings.name){
          case 'chat':
            return MaterialPageRoute(builder: (context)=>const chat_page(),settings: settings);
          
          // case 'edit_account':
          // return MaterialPageRoute(builder: (context)=>const edit_account(),settings: settings);

          // case 'order_history':
          // return MaterialPageRoute(builder: (context)=>const order_history(),settings: settings);
         

          default:
            throw Exception('invalid route');
        }     
         },
    );
  }

  @override
  
  bool get wantKeepAlive => true;
}