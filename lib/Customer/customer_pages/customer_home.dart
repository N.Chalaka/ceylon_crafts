// ignore_for_file: camel_case_types

import 'package:ceylon_crafts/sizeConfig.dart';
import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import 'main_tab_pages/account_page_builder.dart';
import 'main_tab_pages/cart_page_builder.dart';
import 'main_tab_pages/chat_page_builder.dart';
import 'main_tab_pages/shops_page_builder.dart';
import 'sub_pages/items/items_page.dart';

class customer_home extends StatefulWidget {
  const customer_home({Key? key}) : super(key: key);

  @override
  _customer_homeState createState() => _customer_homeState();
}

class _customer_homeState extends State<customer_home>
    with AutomaticKeepAliveClientMixin {
  late PageController _pageController;

  int _selectedIndex = 0;
  void _onPageChanged(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  void _ontap(int selectedIndex) {
    _pageController.jumpToPage(selectedIndex);
  }

  @override
  void initState() {
    super.initState();
    _pageController = PageController();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    SizeConfig().init(context);
    return Scaffold(
      body: PageView(
        physics: const NeverScrollableScrollPhysics(),
        controller: _pageController,
        onPageChanged: _onPageChanged,
        children: const [
          items_page(),
          cart_page_builder(),
          shops_page_builder(),
          account_page_builder(),
          chat_page_builder()
        ],
      ),
      bottomNavigationBar: CurvedNavigationBar(
        height: SizeConfig.blockSizeVertical * 7 > 75
            ? 75
            : SizeConfig.blockSizeVertical * 6,
        color: Colors.grey.shade300,
        backgroundColor: Colors.white,
        buttonBackgroundColor: Colors.green[300],
        items: [
          Padding(
            padding: EdgeInsets.all(SizeConfig.blockSizeVertical * 0.5),
            child: SvgPicture.asset(
              "assets/icons/item.svg",
              width: SizeConfig.blockSizeVertical * 7 > 75
                  ? 45
                  : SizeConfig.blockSizeVertical * 3,
              height: SizeConfig.blockSizeVertical * 7 > 75
                  ? 45
                  : SizeConfig.blockSizeVertical * 3,
              color: Colors.brown,
            ),
          ),
          Padding(
            padding: EdgeInsets.all(SizeConfig.blockSizeVertical *0.5),
            child: SvgPicture.asset(
              "assets/icons/cart.svg",
              width: SizeConfig.blockSizeVertical * 7 > 75
                  ? 45
                  : SizeConfig.blockSizeVertical * 3,
              height: SizeConfig.blockSizeVertical * 7 > 75
                  ? 45
                  : SizeConfig.blockSizeVertical * 3,
              color: Colors.brown,
            ),
          ),
          Padding(
            padding: EdgeInsets.all(SizeConfig.blockSizeVertical *0.5),
            child: SvgPicture.asset(
              "assets/icons/shop.svg",
              width: SizeConfig.blockSizeVertical * 7 > 75
                  ? 45
                  : SizeConfig.blockSizeVertical * 3,
              height: SizeConfig.blockSizeVertical * 7 > 75
                  ? 45
                  : SizeConfig.blockSizeVertical * 3,
              color: Colors.brown,
            ),
          ),
          Padding(
            padding: EdgeInsets.all(SizeConfig.blockSizeVertical* 0.5),
            child: SvgPicture.asset(
              "assets/icons/account.svg",
              width: SizeConfig.blockSizeVertical * 7 > 75
                  ? 45
                  : SizeConfig.blockSizeVertical * 3,
              height: SizeConfig.blockSizeVertical * 7 > 75
                  ? 45
                  : SizeConfig.blockSizeVertical * 3,
              color: Colors.brown,
            ),
          ),
          Padding(
            padding: EdgeInsets.all(SizeConfig.blockSizeVertical*0.5),
            child: SvgPicture.asset(
              "assets/icons/chat.svg",
              width: SizeConfig.blockSizeVertical * 7 > 75
                  ? 45
                  : SizeConfig.blockSizeVertical * 3,
              height: SizeConfig.blockSizeVertical * 7 > 75
                  ? 45
                  : SizeConfig.blockSizeVertical * 3,
            ),
          )
        ],
        onTap: _ontap,
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
