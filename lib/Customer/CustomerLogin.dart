// ignore_for_file: file_names
import 'package:ceylon_crafts/Customer/customer_pages/customer_home.dart';
import 'package:ceylon_crafts/sizeConfig.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

//import 'customBoxShadow.dart';
import 'customerSignup.dart';

class CustomerLogin extends StatefulWidget {
  const CustomerLogin({Key? key}) : super(key: key);

  @override
  _CustomerLoginState createState() => _CustomerLoginState();
}

class _CustomerLoginState extends State<CustomerLogin> {
  navigateToShops() async {
    await Navigator.push(context,
        MaterialPageRoute(builder: (context) => const customer_home()));
  }

  late bool isPasswordHidden;

  @override
  void initState() {
    super.initState();
    isPasswordHidden = true;
  }

  TextEditingController password = TextEditingController();
  final GlobalKey<FormState> _formkey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return GestureDetector(
      onTap: () {
        FocusScopeNode currentFocus = FocusScope.of(context);
        if (!currentFocus.hasPrimaryFocus) {
          currentFocus.unfocus();
        }
      },
      child: Scaffold(
        // resizeToAvoidBottomInset: false,

        body: SafeArea(
          child: Container(
            width: SizeConfig.screenWidth,
            height: SizeConfig.screenHeight - SizeConfig.safeAreaVertical,
            color: Colors.white,
            child: Stack(children: [
              Positioned(
                child: SvgPicture.asset(
                  'assets/side-image.svg',
                  height: SizeConfig.screenWidth / 2,
                  width: SizeConfig.screenWidth / 2,
                ),
                top: 0,
                right: 0,
              ),
              Positioned(
                child: SvgPicture.asset(
                  'assets/bottom-image.svg',
                  height: SizeConfig.screenWidth / 3,
                  width: 2 * SizeConfig.screenWidth / 3,
                ),
                bottom: 0,
                left: SizeConfig.screenWidth / 6,
              ),
              Container(
                child: SingleChildScrollView(
                  reverse: true,
                  child: Column(
                    children: [
                      Container(
                        width: SizeConfig.screenWidth,
                        //height: SizeConfig.blockSizeVertical * 12,
                        color: Colors.transparent,
                        child: Padding(
                          padding:
                              EdgeInsets.all(SizeConfig.blockSizeVertical * 2),
                          child: Row(
                            mainAxisSize: MainAxisSize.max,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                'Login',
                                style: TextStyle(
                                    fontFamily: 'Inspiration',
                                    color: Colors.brown,
                                    fontWeight: FontWeight.w400,
                                    fontSize:
                                        SizeConfig.blockSizeVertical * 5.5),
                              ),
                            ],
                          ),
                        ),
                      ),
                      Container(
                          margin: EdgeInsets.only(
                              top: SizeConfig.blockSizeHorizontal * 15),
                          // height: SizeConfig.blockSizeVertical * 40,
                          width: SizeConfig.blockSizeHorizontal * 80,
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(
                                  SizeConfig.blockSizeVertical * 4),
                              border: Border.all(
                                color: Colors.brown,
                                width: 3,
                              ),
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.brown.withOpacity(0.5),
                                  spreadRadius: 0,
                                  blurRadius: 7,
                                  offset: const Offset(0, 3),
                                )
                              ]),
                          child: Form(
                              key: _formkey,
                              child: Column(children: [
                                Container(
                                  width: SizeConfig.blockSizeHorizontal * 80,
                                  //height: SizeConfig.blockSizeVertical * 8,
                                  color: Colors.white.withOpacity(0),
                                  child: Padding(
                                    padding: EdgeInsets.all(
                                        SizeConfig.blockSizeVertical),
                                    child: const Center(
                                        child: Text('Login details :')),
                                  ),
                                ),
                                Container(
                                    margin: const EdgeInsets.all(50),
                                    //height: MediaQuery.of(context).size.height * 0.4,
                                    width: SizeConfig.blockSizeHorizontal * 60,
                                    //height: SizeConfig.blockSizeVertical * 8,
                                    child: Column(children: [
                                      Container(
                                        width:
                                            SizeConfig.blockSizeHorizontal * 60,
                                        //height: SizeConfig.blockSizeVertical * 8,
                                        color: Colors.transparent,
                                        child: Padding(
                                          padding: EdgeInsets.all(
                                              SizeConfig.blockSizeVertical * 1),
                                          child: TextFormField(
                                            // style: TextStyle(
                                            //   fontSize:
                                            //       SizeConfig.blockSizeVertical *
                                            //           2,
                                            // ),
                                            decoration: InputDecoration(
                                              // contentPadding: EdgeInsets.all(
                                              //     SizeConfig.blockSizeVertical),
                                              fillColor: Colors.white,
                                              filled: true,
                                              prefixIcon:
                                                  const Icon(Icons.person),
                                              border: OutlineInputBorder(
                                                borderRadius:
                                                    BorderRadius.circular(20),
                                              ),

                                              hintText: 'Enter username',
                                              hintStyle: const TextStyle(),

                                              // prefixIcon: const Icon(Icons.person,
                                              //     color: Colors.brown),
                                              // border: OutlineInputBorder(
                                              //   borderSide: BorderSide.none,
                                              //   borderRadius: BorderRadius.circular(
                                              //       SizeConfig.blockSizeVertical *
                                              //           4),
                                              // ),
                                              labelText: 'Username',

                                              // fillColor: Colors.grey.shade500,
                                            ),
                                            validator: (value) {
                                              if (value!.isEmpty) {
                                                return 'Please a Enter user name';
                                              }
                                              return null;
                                            },
                                          ),
                                        ),
                                      ),
                                      Container(
                                        width:
                                            SizeConfig.blockSizeHorizontal * 60,
                                        //height: SizeConfig.blockSizeVertical * 8,
                                        child: Padding(
                                          padding: EdgeInsets.all(
                                              SizeConfig.blockSizeVertical * 1),
                                          child: TextFormField(
                                            // style: TextStyle(
                                            //     fontSize: SizeConfig
                                            //             .blockSizeVertical *
                                            //         2),
                                            obscureText: isPasswordHidden,
                                            controller: password,
                                            decoration: InputDecoration(
                                                // contentPadding: EdgeInsets.all(
                                                //     SizeConfig.blockSizeVertical),
                                                // fillColor: Colors.grey.shade400,
                                                // filled: true,
                                                // floatingLabelBehavior:
                                                //     FloatingLabelBehavior.never,
                                                hintText: 'Enter password',
                                                //
                                                fillColor: Colors.white,
                                                filled: true,
                                                border: OutlineInputBorder(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            20)),
                                                prefixIcon:
                                                    const Icon(Icons.lock),
                                                labelText: 'Password',
                                                suffixIcon: IconButton(
                                                    onPressed: showhidePassword,
                                                    icon: isPasswordHidden
                                                        ? const Icon(Icons
                                                            .visibility_off)
                                                        : const Icon(
                                                            Icons.visibility))

                                                // fillColor: Colors.grey.shade500,
                                                ),
                                            validator: (value) {
                                              if (value!.isEmpty) {
                                                return 'Please a Enter Password';
                                              }
                                              return null;
                                            },
                                          ),
                                        ),
                                      ),
                                      Padding(
                                        padding: EdgeInsets.only(
                                            top: SizeConfig.blockSizeVertical *
                                                3),
                                        child: Container(
                                          width:
                                              SizeConfig.blockSizeHorizontal *
                                                  25,
                                          // height: SizeConfig.blockSizeVertical * 6,
                                          child: ElevatedButton(
                                            onPressed: () {
                                              if (_formkey.currentState!
                                                  .validate()) {
                                                navigateToShops();
                                                //print("successful");
                                                return;
                                              } else {
                                                print("UnSuccessfull");
                                              }
                                            },
                                            style: ButtonStyle(
                                              backgroundColor:
                                                  MaterialStateProperty.all(
                                                      Colors.brown),
                                              shape: MaterialStateProperty.all(
                                                  RoundedRectangleBorder(
                                                      borderRadius: BorderRadius
                                                          .circular(SizeConfig
                                                                  .blockSizeVertical *
                                                              1.0))),
                                            ),
                                            child: Text(
                                              'Login',
                                              style: TextStyle(
                                                color: Colors.white,
                                                fontSize: SizeConfig
                                                        .blockSizeVertical *
                                                    2,
                                                fontWeight: FontWeight.w500,
                                              ),
                                            ),
                                          ),
                                        ),
                                      )
                                    ])),
                              ]))),
                      Padding(
                          padding: EdgeInsets.only(
                              top: SizeConfig.blockSizeVertical * 2),
                          child: Container(
                              width: SizeConfig.screenWidth,
                              height: SizeConfig.blockSizeVertical * 10,
                              child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Padding(
                                      padding: EdgeInsets.all(
                                          SizeConfig.blockSizeVertical),
                                      child: Text(
                                        'Don\'t have an account?',
                                        style: TextStyle(
                                            color: Colors.grey.shade500),
                                      ),
                                    ),
                                    TextButton(
                                        child: const Text('Signup'),
                                        onPressed: () {
                                          //BuildContext context;
                                          Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (context) =>
                                                      const CustomerSignup()));
                                        })
                                  ])))
                    ],
                  ),
                ),
              ),
            ]),
          ),
        ),
      ),
    );
  }

  void showhidePassword() {
    setState(() {
      if (isPasswordHidden == true) {
        isPasswordHidden = false;
      } else {
        isPasswordHidden = true;
      }
    });
  }
}
