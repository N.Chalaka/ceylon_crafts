// ignore_for_file: file_names

import 'package:ceylon_crafts/Customer/customer_pages/customer_home.dart';

import 'package:ceylon_crafts/sizeConfig.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import 'CustomerLogin.dart';
import 'customBoxShadow.dart';

class CustomerSignup extends StatefulWidget {
  const CustomerSignup({Key? key}) : super(key: key);

  @override
  _CustomerSignupState createState() => _CustomerSignupState();
}

class _CustomerSignupState extends State<CustomerSignup> {
  navigateToShops() async {
    await Navigator.push(context,
        MaterialPageRoute(builder: (context) => const customer_home()));
  }

  late bool isPasswordHiddenOne;
  late bool isPasswordHiddenTwo;

  @override
  void initState() {
    super.initState();
    isPasswordHiddenOne = true;
    isPasswordHiddenTwo = true;
  }

  late String First_name, Last_name, email, phone;
  //TextController to read text entered in text field
  TextEditingController password = TextEditingController();
  TextEditingController confirmpassword = TextEditingController();
  final GlobalKey<FormState> _formkey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);

    return GestureDetector(
      onTap: () {
        FocusScopeNode currentFocus = FocusScope.of(context);
        if (!currentFocus.hasPrimaryFocus) {
          currentFocus.unfocus();
        }
      },
      child: Scaffold(
        // resizeToAvoidBottomInset: false,

        body: SafeArea(
          child: Container(
            width: SizeConfig.screenWidth,
            height: SizeConfig.screenHeight - SizeConfig.safeAreaVertical,
            color: Colors.white,
            child: Stack(children: [
              Positioned(
                child: SvgPicture.asset(
                  'assets/side-image.svg',
                  height: SizeConfig.screenWidth / 2,
                  width: SizeConfig.screenWidth / 2,
                ),
                top: 0,
                right: 0,
              ),
              Container(
                child: SingleChildScrollView(
                  reverse: true,
                  child: Column(
                    children: [
                      Container(
                        width: SizeConfig.screenWidth,
                        //height: SizeConfig.blockSizeVertical * 12,
                        color: Colors.transparent,
                        child: Padding(
                          padding:
                              EdgeInsets.all(SizeConfig.blockSizeVertical * 2),
                          child: Text(
                            'Signup',
                            style: TextStyle(
                                color: Colors.brown,
                                fontWeight: FontWeight.w400,
                                fontFamily: 'Hind',
                                fontSize: SizeConfig.blockSizeVertical * 6.5),
                          ),
                        ),
                      ),
                      Container(
                          margin: EdgeInsets.only(
                              top: SizeConfig.blockSizeHorizontal * 10),
                          //height: SizeConfig.blockSizeVertical * 70,
                          width: SizeConfig.blockSizeHorizontal * 80,
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(
                                  SizeConfig.blockSizeVertical * 4),
                              border: Border.all(
                                color: Colors.brown,
                                width: 3,
                              ),

                              // ignore: prefer_const_constructors
                              boxShadow: [
                                CustomBoxShadow(
                                  color: Colors.grey.shade600,
                                  blurRadius: 7,
                                  spreadRadius: 0,
                                  blurStyle: BlurStyle.outer,
                                  offset: const Offset(0, 0),
                                )
                              ]),
                          child: Form(
                              key: _formkey,
                              child: Column(children: [
                                Container(
                                  width: SizeConfig.blockSizeHorizontal * 80,
                                  //height: SizeConfig.blockSizeVertical * 8,
                                  color: Colors.white.withOpacity(0),
                                  child: Padding(
                                    padding: EdgeInsets.all(
                                        SizeConfig.blockSizeVertical),
                                    child: const Center(
                                        child: Text('Signup details :')),
                                  ),
                                ),
                                Container(
                                    width: SizeConfig.blockSizeHorizontal * 60,
                                    //height: SizeConfig.blockSizeVertical * 8,
                                    child: Column(children: [
                                      Container(
                                        width:
                                            SizeConfig.blockSizeHorizontal * 60,
                                        // height:
                                        //     SizeConfig.blockSizeVertical * 80,
                                        color: Colors.transparent,
                                        child: Padding(
                                          padding: EdgeInsets.all(
                                              SizeConfig.blockSizeVertical * 1),
                                          child: TextFormField(
                                            // style: TextStyle(
                                            //   fontSize:
                                            //       SizeConfig.blockSizeVertical *
                                            //           2,
                                            // ),
                                            cursorHeight: 32.0,
                                            decoration: InputDecoration(
                                              fillColor: Colors.white,
                                              filled: true,
                                              border: OutlineInputBorder(
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          20)),
                                              hintText: 'Enter first name',
                                              hintStyle: const TextStyle(),

                                              prefixIcon: const Icon(
                                                Icons.person,
                                                color: Colors.brown,
                                              ),

                                              labelText: 'First name',

                                              // fillColor: Colors.grey.shade500,
                                            ),
                                            validator: (value) {
                                              if (value!.isEmpty) {
                                                return 'Please Enter First Name';
                                              }
                                              return null;
                                            },
                                            onSaved: (value) {
                                              First_name = value!;
                                            },
                                          ),
                                        ),
                                      ),
                                      Container(
                                        width:
                                            SizeConfig.blockSizeHorizontal * 60,
                                        // height:
                                        //     SizeConfig.blockSizeVertical * 8,
                                        child: Padding(
                                          padding: EdgeInsets.all(
                                              SizeConfig.blockSizeVertical * 1),
                                          child: TextFormField(
                                            // style: TextStyle(
                                            //     fontSize: SizeConfig
                                            //             .blockSizeVertical *
                                            //         2),
                                            decoration: InputDecoration(
                                              fillColor: Colors.white,
                                              filled: true,
                                              border: OutlineInputBorder(
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          20)),
                                              hintText: 'Enter last name',
                                              prefixIcon: const Icon(
                                                  Icons.person,
                                                  color: Colors.brown),

                                              labelText: 'Last name',
                                              // fillColor: Colors.grey.shade500,
                                            ),
                                            validator: (value) {
                                              if (value!.isEmpty) {
                                                return 'Please Enter Last Name';
                                              }
                                              return null;
                                            },
                                            onSaved: (value) {
                                              Last_name = value!;
                                            },
                                          ),
                                        ),
                                      ),
                                      Container(
                                        width:
                                            SizeConfig.blockSizeHorizontal * 60,
                                        // height:
                                        //     SizeConfig.blockSizeVertical * 8,
                                        child: Padding(
                                          padding: EdgeInsets.all(
                                              SizeConfig.blockSizeVertical * 1),
                                          child: TextFormField(
                                            // style: TextStyle(
                                            //     fontSize: SizeConfig
                                            //             .blockSizeVertical *
                                            //         2),
                                            decoration: InputDecoration(
                                              fillColor: Colors.white,
                                              filled: true,
                                              border: OutlineInputBorder(
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          20)),
                                              hintText: 'Enter email',

                                              prefixIcon: const Icon(
                                                  Icons.email,
                                                  color: Colors.brown),

                                              labelText: 'Email',
                                              // fillColor: Colors.grey.shade500,
                                            ),
                                            validator: (value) {
                                              if (value!.isEmpty) {
                                                return 'Please a Enter email';
                                              }
                                              if (!RegExp(
                                                      "^[a-zA-Z0-9+_.-]+@[a-zA-Z0-9.-]+.[a-z]")
                                                  .hasMatch(value)) {
                                                return 'Please a valid Email';
                                              }
                                              return null;
                                            },
                                            onSaved: (value) {
                                              email = value!;
                                            },
                                          ),
                                        ),
                                      ),
                                      Container(
                                        width:
                                            SizeConfig.blockSizeHorizontal * 60,
                                        // height:
                                        //     SizeConfig.blockSizeVertical * 8,
                                        child: Padding(
                                          padding: EdgeInsets.all(
                                              SizeConfig.blockSizeVertical * 1),
                                          child: TextFormField(
                                            // style: TextStyle(
                                            //     fontSize: SizeConfig
                                            //             .blockSizeVertical *
                                            //         2),
                                            obscureText: isPasswordHiddenOne,
                                            controller: password,
                                            decoration: InputDecoration(
                                                fillColor: Colors.white,
                                                filled: true,
                                                border: OutlineInputBorder(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            20)),
                                                hintText: 'Enter password',
                                                prefixIcon: const Icon(
                                                    Icons.lock,
                                                    color: Colors.brown),
                                                labelText: 'Password',
                                                suffixIcon: IconButton(
                                                  icon: isPasswordHiddenOne
                                                      ? const Icon(
                                                          Icons.visibility_off)
                                                      : const Icon(
                                                          Icons.visibility),
                                                  onPressed:
                                                      showhidePasswordOne,
                                                )

                                                // fillColor: Colors.grey.shade500,
                                                ),
                                            validator: (value) {
                                              if (value!.isEmpty) {
                                                return 'Please a Enter Password';
                                              }
                                              return null;
                                            },
                                          ),
                                        ),
                                      ),
                                      Container(
                                        width:
                                            SizeConfig.blockSizeHorizontal * 60,
                                        // height:
                                        //     SizeConfig.blockSizeVertical * 8,
                                        child: Padding(
                                          padding: EdgeInsets.all(
                                              SizeConfig.blockSizeVertical * 1),
                                          child: TextFormField(
                                            // style: TextStyle(
                                            //     fontSize: SizeConfig
                                            //             .blockSizeVertical *
                                            //         2),
                                            obscureText: isPasswordHiddenTwo,
                                            controller: confirmpassword,
                                            decoration: InputDecoration(
                                                fillColor: Colors.white,
                                                filled: true,
                                                border: OutlineInputBorder(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            20)),
                                                hintText: 'Confirm password',
                                                prefixIcon: const Icon(
                                                    Icons.lock,
                                                    color: Colors.brown),
                                                labelText: 'Confirm password',
                                                suffixIcon: IconButton(
                                                  icon: isPasswordHiddenTwo
                                                      ? const Icon(
                                                          Icons.visibility_off)
                                                      : const Icon(
                                                          Icons.visibility),
                                                  onPressed:
                                                      showhidePasswordTwo,
                                                )

                                                // fillColor: Colors.grey.shade500,

                                                ),
                                            validator: (value) {
                                              if (value!.isEmpty) {
                                                return 'Please re-enter password';
                                              }
                                              print(password.text);
                                              print(confirmpassword.text);
                                              if (password.text !=
                                                  confirmpassword.text) {
                                                return "Password does not match";
                                              }
                                              return null;
                                            },
                                          ),
                                        ),
                                      ),
                                      Container(
                                        width:
                                            SizeConfig.blockSizeHorizontal * 60,
                                        // height:
                                        //     SizeConfig.blockSizeVertical * 8,
                                        child: Padding(
                                          padding: EdgeInsets.all(
                                              SizeConfig.blockSizeVertical * 1),
                                          child: TextFormField(
                                            // style: TextStyle(
                                            //     fontSize: SizeConfig
                                            //             .blockSizeVertical *
                                            //         2),
                                            decoration: InputDecoration(
                                                fillColor: Colors.white,
                                                filled: true,
                                                border: OutlineInputBorder(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            20)),
                                                hintText: 'Enter telephone no.',
                                                prefixIcon: const Icon(
                                                    Icons.phone,
                                                    color: Colors.brown),
                                                labelText: 'Telephone no.'

                                                // fillColor: Colors.grey.shade500,

                                                ),
                                            validator: (value) {
                                              if (value!.isEmpty) {
                                                return 'Please enter phone no ';
                                              }
                                              if (!RegExp("^[0-9+-]+")
                                                  .hasMatch(value)) {
                                                return 'Please a valid phone no';
                                              }
                                              return null;
                                            },
                                            onSaved: (value) {
                                              phone = value!;
                                            },
                                          ),
                                        ),
                                      ),
                                      Padding(
                                        padding: EdgeInsets.only(
                                            top: SizeConfig.blockSizeVertical *
                                                3),
                                        child: Container(
                                          width:
                                              SizeConfig.blockSizeHorizontal *
                                                  25,
                                          // height:
                                          //     SizeConfig.blockSizeVertical *
                                          //         6,
                                          child: ElevatedButton(
                                            onPressed: () {
                                              //navigateToShops();
                                              if (_formkey.currentState!
                                                  .validate()) {
                                                navigateToShops();
                                                //print("successful");
                                                return;
                                              } else {
                                                print("UnSuccessfull");
                                              }
                                            },
                                            style: ButtonStyle(
                                              backgroundColor:
                                                  MaterialStateProperty.all(
                                                      Colors.brown),
                                              shape: MaterialStateProperty.all(
                                                  RoundedRectangleBorder(
                                                      borderRadius: BorderRadius
                                                          .circular(SizeConfig
                                                                  .blockSizeVertical *
                                                              1.0))),
                                            ),
                                            child: Text(
                                              'Signup',
                                              style: TextStyle(
                                                color: Colors.white,
                                                fontSize: SizeConfig
                                                        .blockSizeVertical *
                                                    2,
                                                fontWeight: FontWeight.w500,
                                              ),
                                            ),
                                          ),
                                        ),
                                      )
                                    ])),
                              ]))),
                      Padding(
                          padding: EdgeInsets.only(
                              top: SizeConfig.blockSizeVertical * 2),
                          child: Container(
                              width: SizeConfig.screenWidth,
                              //height: SizeConfig.blockSizeVertical * 10,
                              child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Padding(
                                      padding: EdgeInsets.all(
                                          SizeConfig.blockSizeVertical),
                                      child: Text(
                                        'Already have an account?',
                                        style: TextStyle(
                                            color: Colors.grey.shade500),
                                      ),
                                    ),
                                    TextButton(
                                        child: const Text('Login'),
                                        onPressed: () {
                                          //BuildContext context;
                                          Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (context) =>
                                                      const CustomerLogin()));
                                        })
                                  ]))),
                    ],
                  ),
                ),
              )
            ]),
          ),
        ),
      ),
    );
  }

  void showhidePasswordOne() {
    setState(() {
      if (isPasswordHiddenOne == true) {
        isPasswordHiddenOne = false;
      } else {
        isPasswordHiddenOne = true;
      }
    });
  }

  void showhidePasswordTwo() {
    setState(() {
      if (isPasswordHiddenTwo == true) {
        isPasswordHiddenTwo = false;
      } else {
        isPasswordHiddenTwo = true;
      }
    });
  }
}
