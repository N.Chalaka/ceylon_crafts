// ignore: file_names

// ignore_for_file: empty_constructor_bodies, file_names, duplicate_ignore, unnecessary_this

import 'package:flutter/material.dart';

class CustomBoxShadow extends BoxShadow{
  final BlurStyle blurStyle;
  

   const CustomBoxShadow ({
    Color color = Colors.grey,
    Offset offset = Offset.zero,
    double blurRadius = 0.0,
    double spreadRadius = 0.0,
    this.blurStyle = BlurStyle.normal,
  }):super(color: color,offset: offset,blurRadius: blurRadius,spreadRadius: spreadRadius);

@override
  Paint toPaint(){
  final Paint result = Paint()
  ..color = color
  ..maskFilter = MaskFilter.blur(this.blurStyle,blurSigma);
  assert((){
    if(debugDisableShadows) {
      result.maskFilter = null;
    }
    return true;
  }());
  return result;
}

}