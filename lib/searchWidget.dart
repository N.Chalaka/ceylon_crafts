
// ignore_for_file: file_names, camel_case_types

import 'package:ceylon_crafts/sizeConfig.dart';
import 'package:flutter/material.dart';



class searchWidget extends StatefulWidget {
  
  final String text;
  final ValueChanged<String> onChanged;
  final String hintText;
  final Color bordercolor;
  const searchWidget({ Key? key ,
      required this.text,
      required this.onChanged,
      required this.hintText,
      required this.bordercolor
  }) : super(key: key);

  @override
  _searchWidgetState createState() => _searchWidgetState();
}


class _searchWidgetState extends State<searchWidget> {

  final controller = TextEditingController();


  @override
  
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    final styleActive = TextStyle(color: Colors.brown,fontSize: SizeConfig.blockSizeVertical*2);
    final styleHint = TextStyle(color: Colors.grey.shade500,fontSize: SizeConfig.blockSizeVertical*2);
    final style = widget.text.isEmpty ? styleHint:styleActive;
    return Container(
     
      //height: SizeConfig.blockSizeVertical*6.5,
      margin: EdgeInsets.fromLTRB(SizeConfig.blockSizeHorizontal, SizeConfig.blockSizeVertical*1, SizeConfig.blockSizeHorizontal, SizeConfig.blockSizeVertical*1),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(SizeConfig.blockSizeVertical*5),
        color: Colors.white,
         
        border: Border.all(color: widget.bordercolor)
      ),
      //padding: EdgeInsets.symmetric(horizontal: SizeConfig.blockSizeHorizontal,
      
     // ),
      child: TextField(
        controller: controller,
        decoration: InputDecoration(
          icon: Container(
            margin: EdgeInsets.only(left: SizeConfig.blockSizeHorizontal*3,),
           // width: SizeConfig.blockSizeVertical*5,
           // height: SizeConfig.blockSizeVertical*5,
          
            child: Icon(Icons.search,color: style.color,size: SizeConfig.blockSizeVertical*3,)),
          suffixIcon: widget.text.isNotEmpty ? GestureDetector(
            child: Icon(Icons.close,color:style.color),
            onTap: (){
              controller.clear();
              widget.onChanged('');
              FocusScope.of(context).requestFocus(FocusNode());

               
            },
          ):null,
          hintText: widget.hintText,
          hintStyle: style,
          border: InputBorder.none,

        ),
        style: style,
        onChanged: widget.onChanged,
        ),
    );
  }
}

