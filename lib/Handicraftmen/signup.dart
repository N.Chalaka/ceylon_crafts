// ignore_for_file: prefer_const_constructors, non_constant_identifier_names, deprecated_member_use, avoid_print

import 'package:ceylon_crafts/Customer/customBoxShadow.dart';
import 'package:ceylon_crafts/Items/itemBuilder.dart';
import 'package:ceylon_crafts/Items/itemPage.dart';
import 'package:ceylon_crafts/Items/itemTab.dart';
import 'package:ceylon_crafts/sizeConfig.dart';
import 'package:flutter/material.dart';
import 'package:ceylon_crafts/Handicraftmen/login.dart';

import 'package:flutter_svg/svg.dart';

class Signup extends StatefulWidget {
  const Signup({Key? key}) : super(key: key);

  @override
  _SignupState createState() => _SignupState();
}

class _SignupState extends State<Signup> {
  navigateToItemTab() async {
    await Navigator.push(
        context, MaterialPageRoute(builder: (context) => const itemTab()));
  }

  late bool isPasswordHiddenOne;
  late bool isPasswordHiddenTwo;

  @override
  void initState() {
    super.initState();
    isPasswordHiddenOne = true;
    isPasswordHiddenTwo = true;
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return GestureDetector(
      onTap: () {
        FocusScopeNode currentFocus = FocusScope.of(context);
        if (!currentFocus.hasPrimaryFocus) {
          currentFocus.unfocus();
        }
      },
      child: Scaffold(
        // resizeToAvoidBottomInset: false,

        body: SafeArea(
          child: SingleChildScrollView(
            reverse: true,
            child: Column(
              children: [
                Container(
                  width: SizeConfig.screenWidth,
                  height: SizeConfig.screenHeight - SizeConfig.safeAreaVertical,
                  color: Colors.white,
                  child: Stack(children: [
                    Positioned(
                      child: SvgPicture.asset(
                        'assets/side-image.svg',
                        height: SizeConfig.screenWidth / 2,
                        width: SizeConfig.screenWidth / 2,
                      ),
                      top: 0,
                      right: 0,
                    ),
                    Positioned(
                      child: Container(
                        width: SizeConfig.screenWidth,
                        height: SizeConfig.blockSizeVertical * 12,
                        color: Colors.transparent,
                        child: Padding(
                          padding:
                              EdgeInsets.all(SizeConfig.blockSizeVertical * 2),
                          child: Text(
                            'Signup',
                            style: TextStyle(
                                color: Colors.brown,
                                fontWeight: FontWeight.w400,
                                fontSize: SizeConfig.blockSizeVertical * 6.5),
                          ),
                        ),
                      ),
                    ),
                    Positioned(
                        top: SizeConfig.blockSizeVertical * 14,
                        left: SizeConfig.blockSizeHorizontal * 10,
                        child: Container(
                            height: SizeConfig.blockSizeVertical * 75,
                            width: SizeConfig.blockSizeHorizontal * 80,
                            decoration: BoxDecoration(
                                color: Colors.transparent,
                                borderRadius: BorderRadius.circular(
                                    SizeConfig.blockSizeVertical * 4),
                                border: Border.all(
                                  color: Colors.brown,
                                  width: 3,
                                ),

                                // ignore: prefer_const_constructors
                                boxShadow: [
                                  CustomBoxShadow(
                                    color: Colors.grey.shade700,
                                    blurRadius: 7,
                                    spreadRadius: 0,
                                    blurStyle: BlurStyle.outer,
                                    offset: const Offset(0, 0),
                                  )
                                ]),
                            child: Padding(
                              padding: EdgeInsets.only(
                                  top: SizeConfig.blockSizeVertical * 2,
                                  right: SizeConfig.blockSizeHorizontal * 1),
                              child: Scrollbar(
                                showTrackOnHover: true,
                                thickness: SizeConfig.blockSizeHorizontal * 2,
                                //isAlwaysShown: true,
                                radius: Radius.circular(
                                    SizeConfig.blockSizeVertical * 20),
                                child: Form(
                                    child: Column(children: [
                                  Expanded(
                                    child: SingleChildScrollView(
                                      child: Column(
                                        children: [
                                          Container(
                                            width:
                                                SizeConfig.blockSizeHorizontal *
                                                    80,
                                            height:
                                                SizeConfig.blockSizeVertical *
                                                    6,
                                            color: Colors.white.withOpacity(0),
                                            child: Padding(
                                              padding:
                                                  EdgeInsets.only(bottom: 0),
                                              child: const Center(
                                                  child: Text(
                                                'Profile details :',
                                                style: TextStyle(
                                                  color: Colors.brown,
                                                  fontWeight: FontWeight.w400,
                                                ),
                                              )),
                                            ),
                                          ),
                                          Container(
                                            width:
                                                SizeConfig.blockSizeHorizontal *
                                                    60,
                                            height:
                                                SizeConfig.blockSizeVertical *
                                                    8,
                                            color: Colors.transparent,
                                            child: Padding(
                                              padding: EdgeInsets.all(
                                                  SizeConfig.blockSizeVertical *
                                                      1),
                                              child: TextFormField(
                                                style: TextStyle(
                                                  fontSize: SizeConfig
                                                          .blockSizeVertical *
                                                      2,
                                                ),
                                                decoration: InputDecoration(
                                                  contentPadding:
                                                      EdgeInsets.all(SizeConfig
                                                          .blockSizeVertical),
                                                  fillColor:
                                                      Colors.grey.shade400,
                                                  filled: true,
                                                  floatingLabelBehavior:
                                                      FloatingLabelBehavior
                                                          .never,
                                                  hintText: 'Enter first name',
                                                  hintStyle: const TextStyle(),

                                                  prefixIcon: const Icon(
                                                      Icons.person,
                                                      color: Colors.brown),
                                                  border: OutlineInputBorder(
                                                    borderSide: BorderSide.none,
                                                    borderRadius: BorderRadius
                                                        .circular(SizeConfig
                                                                .blockSizeVertical *
                                                            4),
                                                  ),
                                                  labelText: 'First name',

                                                  // fillColor: Colors.grey.shade500,
                                                ),
                                              ),
                                            ),
                                          ),
                                          Container(
                                            width:
                                                SizeConfig.blockSizeHorizontal *
                                                    60,
                                            height:
                                                SizeConfig.blockSizeVertical *
                                                    8,
                                            child: Padding(
                                              padding: EdgeInsets.all(
                                                  SizeConfig.blockSizeVertical *
                                                      1),
                                              child: TextFormField(
                                                style: TextStyle(
                                                    fontSize: SizeConfig
                                                            .blockSizeVertical *
                                                        2),
                                                decoration: InputDecoration(
                                                  contentPadding:
                                                      EdgeInsets.all(SizeConfig
                                                          .blockSizeVertical),
                                                  fillColor:
                                                      Colors.grey.shade400,
                                                  filled: true,
                                                  floatingLabelBehavior:
                                                      FloatingLabelBehavior
                                                          .never,
                                                  hintText: 'Enter last name',
                                                  prefixIcon: const Icon(
                                                      Icons.person,
                                                      color: Colors.brown),
                                                  border: OutlineInputBorder(
                                                      borderSide:
                                                          BorderSide.none,
                                                      borderRadius: BorderRadius
                                                          .circular(SizeConfig
                                                                  .blockSizeVertical *
                                                              4)),
                                                  labelText: 'Last name',
                                                  // fillColor: Colors.grey.shade500,
                                                ),
                                              ),
                                            ),
                                          ),
                                          Container(
                                            width:
                                                SizeConfig.blockSizeHorizontal *
                                                    60,
                                            height:
                                                SizeConfig.blockSizeVertical *
                                                    8,
                                            child: Padding(
                                              padding: EdgeInsets.all(
                                                  SizeConfig.blockSizeVertical *
                                                      1),
                                              child: TextFormField(
                                                style: TextStyle(
                                                    fontSize: SizeConfig
                                                            .blockSizeVertical *
                                                        2),
                                                decoration: InputDecoration(
                                                  contentPadding:
                                                      EdgeInsets.all(SizeConfig
                                                          .blockSizeVertical),
                                                  fillColor:
                                                      Colors.grey.shade400,
                                                  filled: true,
                                                  floatingLabelBehavior:
                                                      FloatingLabelBehavior
                                                          .never,
                                                  hintText: 'Enter email',
                                                  prefixIcon: const Icon(
                                                      Icons.email,
                                                      color: Colors.brown),
                                                  border: OutlineInputBorder(
                                                      borderSide:
                                                          BorderSide.none,
                                                      borderRadius: BorderRadius
                                                          .circular(SizeConfig
                                                                  .blockSizeVertical *
                                                              4)),
                                                  labelText: 'Email',
                                                  // fillColor: Colors.grey.shade500,
                                                ),
                                              ),
                                            ),
                                          ),
                                          Container(
                                            width:
                                                SizeConfig.blockSizeHorizontal *
                                                    60,
                                            height:
                                                SizeConfig.blockSizeVertical *
                                                    8,
                                            child: Padding(
                                              padding: EdgeInsets.all(
                                                  SizeConfig.blockSizeVertical *
                                                      1),
                                              child: TextFormField(
                                                style: TextStyle(
                                                    fontSize: SizeConfig
                                                            .blockSizeVertical *
                                                        2),
                                                obscureText:
                                                    isPasswordHiddenOne,
                                                decoration: InputDecoration(
                                                    contentPadding: EdgeInsets
                                                        .all(SizeConfig
                                                            .blockSizeVertical),
                                                    fillColor:
                                                        Colors.grey.shade400,
                                                    filled: true,
                                                    floatingLabelBehavior:
                                                        FloatingLabelBehavior
                                                            .never,
                                                    hintText: 'Enter password',
                                                    prefixIcon: const Icon(
                                                        Icons.lock,
                                                        color: Colors.brown),
                                                    border: OutlineInputBorder(
                                                        borderSide:
                                                            BorderSide.none,
                                                        borderRadius: BorderRadius
                                                            .circular(SizeConfig
                                                                    .blockSizeVertical *
                                                                4)),
                                                    labelText: 'Password',
                                                    suffixIcon: IconButton(
                                                      icon: isPasswordHiddenOne
                                                          ? const Icon(Icons
                                                              .visibility_off)
                                                          : const Icon(
                                                              Icons.visibility),
                                                      onPressed:
                                                          showhidePasswordOne,
                                                    )

                                                    // fillColor: Colors.grey.shade500,
                                                    ),
                                              ),
                                            ),
                                          ),
                                          Container(
                                            width:
                                                SizeConfig.blockSizeHorizontal *
                                                    60,
                                            height:
                                                SizeConfig.blockSizeVertical *
                                                    8,
                                            child: Padding(
                                              padding: EdgeInsets.all(
                                                  SizeConfig.blockSizeVertical *
                                                      1),
                                              child: TextFormField(
                                                style: TextStyle(
                                                    fontSize: SizeConfig
                                                            .blockSizeVertical *
                                                        2),
                                                obscureText:
                                                    isPasswordHiddenTwo,
                                                decoration: InputDecoration(
                                                    contentPadding: EdgeInsets
                                                        .all(SizeConfig
                                                            .blockSizeVertical),
                                                    fillColor:
                                                        Colors.grey.shade400,
                                                    filled: true,
                                                    floatingLabelBehavior:
                                                        FloatingLabelBehavior
                                                            .never,
                                                    hintText:
                                                        'Confirm password',
                                                    prefixIcon: const Icon(
                                                        Icons.lock,
                                                        color: Colors.brown),
                                                    border: OutlineInputBorder(
                                                        borderSide:
                                                            BorderSide.none,
                                                        borderRadius: BorderRadius
                                                            .circular(SizeConfig
                                                                    .blockSizeVertical *
                                                                4)),
                                                    labelText:
                                                        'Confirm password',
                                                    suffixIcon: IconButton(
                                                      icon: isPasswordHiddenTwo
                                                          ? const Icon(Icons
                                                              .visibility_off)
                                                          : const Icon(
                                                              Icons.visibility),
                                                      onPressed:
                                                          showhidePasswordTwo,
                                                    )
                                                    // fillColor: Colors.grey.shade500,

                                                    ),
                                              ),
                                            ),
                                          ),
                                          Container(
                                            width:
                                                SizeConfig.blockSizeHorizontal *
                                                    60,
                                            height:
                                                SizeConfig.blockSizeVertical *
                                                    8,
                                            child: Padding(
                                              padding: EdgeInsets.all(
                                                  SizeConfig.blockSizeVertical *
                                                      1),
                                              child: TextFormField(
                                                style: TextStyle(
                                                    fontSize: SizeConfig
                                                            .blockSizeVertical *
                                                        2),
                                                decoration: InputDecoration(
                                                    contentPadding: EdgeInsets
                                                        .all(SizeConfig
                                                            .blockSizeVertical),
                                                    fillColor:
                                                        Colors.grey.shade400,
                                                    filled: true,
                                                    floatingLabelBehavior:
                                                        FloatingLabelBehavior
                                                            .never,
                                                    hintText: 'Enter NIC.',
                                                    prefixIcon: const Icon(
                                                        Icons.password,
                                                        color: Colors.brown),
                                                    border: OutlineInputBorder(
                                                        borderSide:
                                                            BorderSide.none,
                                                        borderRadius: BorderRadius
                                                            .circular(SizeConfig
                                                                    .blockSizeVertical *
                                                                4)),
                                                    labelText: 'NIC'

                                                    // fillColor: Colors.grey.shade500,

                                                    ),
                                              ),
                                            ),
                                          ),
                                          Container(
                                            width:
                                                SizeConfig.blockSizeHorizontal *
                                                    60,
                                            height:
                                                SizeConfig.blockSizeVertical *
                                                    8,
                                            child: Padding(
                                              padding: EdgeInsets.all(
                                                  SizeConfig.blockSizeVertical *
                                                      1),
                                              child: TextFormField(
                                                style: TextStyle(
                                                    fontSize: SizeConfig
                                                            .blockSizeVertical *
                                                        2),
                                                decoration: InputDecoration(
                                                    contentPadding:
                                                        EdgeInsets.all(SizeConfig
                                                            .blockSizeVertical),
                                                    fillColor:
                                                        Colors.grey.shade400,
                                                    filled: true,
                                                    floatingLabelBehavior:
                                                        FloatingLabelBehavior
                                                            .never,
                                                    hintText:
                                                        'Enter telephone no.',
                                                    prefixIcon: const Icon(
                                                        Icons.phone,
                                                        color: Colors.brown),
                                                    border: OutlineInputBorder(
                                                        borderSide:
                                                            BorderSide.none,
                                                        borderRadius: BorderRadius
                                                            .circular(SizeConfig
                                                                    .blockSizeVertical *
                                                                4)),
                                                    labelText: 'Telephone no.'

                                                    // fillColor: Colors.grey.shade500,

                                                    ),
                                              ),
                                            ),
                                          ),
                                          Container(
                                            width:
                                                SizeConfig.blockSizeHorizontal *
                                                    80,
                                            height:
                                                SizeConfig.blockSizeVertical *
                                                    6,
                                            color: Colors.white.withOpacity(0),
                                            child: Padding(
                                              padding:
                                                  EdgeInsets.only(bottom: 0),
                                              child: const Center(
                                                  child: Text(
                                                'Bank details :',
                                                style: TextStyle(
                                                  color: Colors.brown,
                                                  fontWeight: FontWeight.w400,
                                                ),
                                              )),
                                            ),
                                          ),
                                          Container(
                                            width:
                                                SizeConfig.blockSizeHorizontal *
                                                    60,
                                            height:
                                                SizeConfig.blockSizeVertical *
                                                    8,
                                            child: Padding(
                                              padding: EdgeInsets.all(
                                                  SizeConfig.blockSizeVertical *
                                                      1),
                                              child: TextFormField(
                                                style: TextStyle(
                                                    fontSize: SizeConfig
                                                            .blockSizeVertical *
                                                        2),
                                                decoration: InputDecoration(
                                                    contentPadding:
                                                        EdgeInsets.all(SizeConfig
                                                            .blockSizeVertical),
                                                    fillColor:
                                                        Colors.grey.shade400,
                                                    filled: true,
                                                    floatingLabelBehavior:
                                                        FloatingLabelBehavior
                                                            .never,
                                                    hintText:
                                                        'Enter Bank Name.',
                                                    prefixIcon: const Icon(
                                                        Icons.account_balance,
                                                        color: Colors.brown),
                                                    border: OutlineInputBorder(
                                                        borderSide:
                                                            BorderSide.none,
                                                        borderRadius: BorderRadius
                                                            .circular(SizeConfig
                                                                    .blockSizeVertical *
                                                                4)),
                                                    labelText: 'Bank'

                                                    // fillColor: Colors.grey.shade500,

                                                    ),
                                              ),
                                            ),
                                          ),
                                          Container(
                                            width:
                                                SizeConfig.blockSizeHorizontal *
                                                    60,
                                            height:
                                                SizeConfig.blockSizeVertical *
                                                    8,
                                            child: Padding(
                                              padding: EdgeInsets.all(
                                                  SizeConfig.blockSizeVertical *
                                                      1),
                                              child: TextFormField(
                                                style: TextStyle(
                                                    fontSize: SizeConfig
                                                            .blockSizeVertical *
                                                        2),
                                                decoration: InputDecoration(
                                                    contentPadding:
                                                        EdgeInsets.all(SizeConfig
                                                            .blockSizeVertical),
                                                    fillColor:
                                                        Colors.grey.shade400,
                                                    filled: true,
                                                    floatingLabelBehavior:
                                                        FloatingLabelBehavior
                                                            .never,
                                                    hintText:
                                                        'Enter Branch Name',
                                                    prefixIcon: const Icon(
                                                        Icons.account_tree,
                                                        color: Colors.brown),
                                                    border: OutlineInputBorder(
                                                        borderSide:
                                                            BorderSide.none,
                                                        borderRadius: BorderRadius
                                                            .circular(SizeConfig
                                                                    .blockSizeVertical *
                                                                4)),
                                                    labelText: 'Branch Name'

                                                    // fillColor: Colors.grey.shade500,

                                                    ),
                                              ),
                                            ),
                                          ),
                                          Container(
                                            width:
                                                SizeConfig.blockSizeHorizontal *
                                                    60,
                                            height:
                                                SizeConfig.blockSizeVertical *
                                                    8,
                                            child: Padding(
                                              padding: EdgeInsets.all(
                                                  SizeConfig.blockSizeVertical *
                                                      1),
                                              child: TextFormField(
                                                style: TextStyle(
                                                    fontSize: SizeConfig
                                                            .blockSizeVertical *
                                                        2),
                                                decoration: InputDecoration(
                                                    contentPadding:
                                                        EdgeInsets.all(SizeConfig
                                                            .blockSizeVertical),
                                                    fillColor:
                                                        Colors.grey.shade400,
                                                    filled: true,
                                                    floatingLabelBehavior:
                                                        FloatingLabelBehavior
                                                            .never,
                                                    hintText:
                                                        'Enter Account no.',
                                                    prefixIcon: const Icon(
                                                        Icons.account_circle,
                                                        color: Colors.brown),
                                                    border: OutlineInputBorder(
                                                        borderSide:
                                                            BorderSide.none,
                                                        borderRadius: BorderRadius
                                                            .circular(SizeConfig
                                                                    .blockSizeVertical *
                                                                4)),
                                                    labelText: 'Account no.'

                                                    // fillColor: Colors.grey.shade500,

                                                    ),
                                              ),
                                            ),
                                          ),
                                          Container(
                                            width:
                                                SizeConfig.blockSizeHorizontal *
                                                    80,
                                            height:
                                                SizeConfig.blockSizeVertical *
                                                    6,
                                            color: Colors.white.withOpacity(0),
                                            child: Padding(
                                              padding:
                                                  EdgeInsets.only(bottom: 0),
                                              child: const Center(
                                                  child: Text(
                                                'Shop details :',
                                                style: TextStyle(
                                                  color: Colors.brown,
                                                  fontWeight: FontWeight.w400,
                                                ),
                                              )),
                                            ),
                                          ),
                                          Container(
                                            width:
                                                SizeConfig.blockSizeHorizontal *
                                                    60,
                                            height:
                                                SizeConfig.blockSizeVertical *
                                                    8,
                                            child: Padding(
                                              padding: EdgeInsets.all(
                                                  SizeConfig.blockSizeVertical *
                                                      1),
                                              child: TextFormField(
                                                style: TextStyle(
                                                    fontSize: SizeConfig
                                                            .blockSizeVertical *
                                                        2),
                                                decoration: InputDecoration(
                                                    contentPadding:
                                                        EdgeInsets.all(SizeConfig
                                                            .blockSizeVertical),
                                                    fillColor:
                                                        Colors.grey.shade400,
                                                    filled: true,
                                                    floatingLabelBehavior:
                                                        FloatingLabelBehavior
                                                            .never,
                                                    hintText: 'Enter shop name',
                                                    prefixIcon: const Icon(
                                                        Icons.add_business,
                                                        color: Colors.brown),
                                                    border: OutlineInputBorder(
                                                        borderSide:
                                                            BorderSide.none,
                                                        borderRadius: BorderRadius
                                                            .circular(SizeConfig
                                                                    .blockSizeVertical *
                                                                4)),
                                                    labelText: 'Shop name'

                                                    // fillColor: Colors.grey.shade500,

                                                    ),
                                              ),
                                            ),
                                          ),
                                          Container(
                                            width:
                                                SizeConfig.blockSizeHorizontal *
                                                    60,
                                            height:
                                                SizeConfig.blockSizeVertical *
                                                    8,
                                            child: Padding(
                                              padding: EdgeInsets.all(
                                                  SizeConfig.blockSizeVertical *
                                                      1),
                                              child: TextFormField(
                                                style: TextStyle(
                                                    fontSize: SizeConfig
                                                            .blockSizeVertical *
                                                        2),
                                                decoration: InputDecoration(
                                                    contentPadding:
                                                        EdgeInsets.all(SizeConfig
                                                            .blockSizeVertical),
                                                    fillColor:
                                                        Colors.grey.shade400,
                                                    filled: true,
                                                    floatingLabelBehavior:
                                                        FloatingLabelBehavior
                                                            .never,
                                                    hintText:
                                                        'Enter shop location',
                                                    prefixIcon: const Icon(
                                                        Icons.location_on,
                                                        color: Colors.brown),
                                                    border: OutlineInputBorder(
                                                        borderSide:
                                                            BorderSide.none,
                                                        borderRadius: BorderRadius
                                                            .circular(SizeConfig
                                                                    .blockSizeVertical *
                                                                4)),
                                                    labelText: 'Location'

                                                    // fillColor: Colors.grey.shade500,

                                                    ),
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                  Padding(
                                    padding: EdgeInsets.only(
                                        bottom: SizeConfig.blockSizeVertical,
                                        top: SizeConfig.blockSizeVertical),
                                    child: Container(
                                      width:
                                          SizeConfig.blockSizeHorizontal * 25,
                                      height: SizeConfig.blockSizeVertical * 6,
                                      child: ElevatedButton(
                                        onPressed: () {
                                          navigateToItemTab();
                                        },
                                        style: ButtonStyle(
                                          backgroundColor:
                                              MaterialStateProperty.all(
                                                  Colors.brown),
                                          shape: MaterialStateProperty.all(
                                              RoundedRectangleBorder(
                                                  borderRadius: BorderRadius
                                                      .circular(SizeConfig
                                                              .blockSizeVertical *
                                                          1.5))),
                                        ),
                                        child: Text(
                                          'Signup',
                                          style: TextStyle(
                                            color: Colors.white,
                                            fontSize:
                                                SizeConfig.blockSizeVertical *
                                                    2,
                                            fontWeight: FontWeight.w500,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                ])),
                              ),
                            ))),
                    Positioned(
                        top: SizeConfig.blockSizeVertical * 85,
                        child: Padding(
                            padding: EdgeInsets.only(
                                top: SizeConfig.blockSizeVertical * 2),
                            child: Container(
                                width: SizeConfig.screenWidth,
                                height: SizeConfig.blockSizeVertical * 10,
                                child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Padding(
                                        padding: EdgeInsets.all(
                                            SizeConfig.blockSizeVertical),
                                        child: Text(
                                          'Already have an account?',
                                          style: TextStyle(
                                              color: Colors.grey.shade500),
                                        ),
                                      ),
                                      TextButton(
                                          child: const Text('Login'),
                                          onPressed: () {
                                            //BuildContext context;
                                            Navigator.push(
                                                context,
                                                MaterialPageRoute(
                                                    builder: (context) =>
                                                        const LogNewWidget()));
                                          })
                                    ]))))
                  ]),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  void showhidePasswordOne() {
    setState(() {
      if (isPasswordHiddenOne == true) {
        isPasswordHiddenOne = false;
      } else {
        isPasswordHiddenOne = true;
      }
    });
  }

  void showhidePasswordTwo() {
    setState(() {
      if (isPasswordHiddenTwo == true) {
        isPasswordHiddenTwo = false;
      } else {
        isPasswordHiddenTwo = true;
      }
    });
  }
}
