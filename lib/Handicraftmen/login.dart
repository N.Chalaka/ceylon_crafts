import 'package:ceylon_crafts/Handicraftmen/signup.dart';
import 'package:ceylon_crafts/Items/itemTab.dart';
import 'package:ceylon_crafts/sizeConfig.dart';
import 'package:flutter/material.dart';

import 'signup.dart';

class LogNewWidget extends StatefulWidget {
  const LogNewWidget({Key? key}) : super(key: key);

  @override
  _LogNewWidgetState createState() => _LogNewWidgetState();
}

class _LogNewWidgetState extends State<LogNewWidget> {
  final scaffoldKey = GlobalKey<ScaffoldState>();

   navigateToItemTab() async {
    await Navigator.push(
        context, MaterialPageRoute(builder: (context) => const itemTab()));
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
        key: scaffoldKey,
        backgroundColor: Colors.white,
        body: SafeArea(
            child: SingleChildScrollView(
          child: Container(
            height: SizeConfig.screenHeight,
            width: SizeConfig.screenWidth,
            // constraints: const BoxConstraints.expand(),
            decoration: const BoxDecoration(
              image: DecorationImage(
                image: AssetImage("assets/LOGIN.png"),
                fit: BoxFit.cover,
              ),
            ),
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.only(top: 50),
                  child: Row(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: const [
                      Text(
                        'Login',
                        style: TextStyle(
                          color: Color(0xFF795548),
                          fontSize: 40,
                          fontFamily: 'Roboto',
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  margin: EdgeInsets.all(50),
                  height: MediaQuery.of(context).size.height * 0.4,
                  decoration: BoxDecoration(
                    border: Border.all(),
                    borderRadius: BorderRadius.circular(20),
                    color: Colors.white,
                    boxShadow: [
                      BoxShadow(
                        color: Colors.brown.withOpacity(0.5),
                        spreadRadius: 5,
                        blurRadius: 7,
                        offset: Offset(0, 3), // changes position of shadow
                      ),
                    ],
                  ),
                  child: Padding(
                    padding: const EdgeInsets.symmetric(
                      vertical: 5.0,
                      horizontal: 20.0,
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Padding(
                          padding: EdgeInsets.all(10),
                          child: TextField(
                            decoration: InputDecoration(
                              fillColor: Colors.white,
                              filled: true,
                              prefixIcon: Icon(Icons.person),
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(20),
                              ),
                              labelText: 'User Name',
                              hintText: 'Enter User Name',
                            ),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.all(10),
                          child: TextField(
                            obscureText: true,
                            decoration: InputDecoration(
                              fillColor: Colors.white,
                              filled: true,
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(20)),
                              labelText: 'Password',
                              hintText: 'Enter Password',
                              prefixIcon: Icon(Icons.lock),
                            ),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(vertical: 16.0),
                          child: RaisedButton(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(8),
                            ),
                            onPressed: () {
                              //    Navigator.of(context).pushNamed(HomePage.tag);
                              navigateToItemTab();
                            },
                            padding: EdgeInsets.all(12),
                            color: Colors.brown,
                            child: Text('Login',
                                style: TextStyle(color: Colors.white)),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    const Text(
                      'Dont\'t have an account ?',
                      style: TextStyle(
                        color: Color(0xFF795548),
                      ),
                    ),
                    TextButton(
                        child: const Text('Signup'),
                        style: TextButton.styleFrom(
                          textStyle: const TextStyle(
                            fontSize: 17,
                            color: Colors.brown,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        onPressed: () {
                          navigateTosignup();
                        })
                  ],
                ),
              ],
            ),
          ),
        )));
  }

  navigateTosignup() async {
    await Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => const Signup(),
      ),
    );
  }
}
