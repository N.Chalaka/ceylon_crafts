import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

// ignore: non_constant_identifier_names
Widget SplashImg() {
  const String img = 'assets/Welcome.svg';
  // ignore: avoid_unnecessary_containers
  return Container(
    // decoration: const BoxDecoration(
    //   image: DecorationImage(
    //     image: AssetImage(img),
    //     fit: BoxFit.fitHeight,
    //   ),
    // ),
    child: Center(
      child:
          SvgPicture.asset(img, alignment: Alignment.center, fit: BoxFit.cover),
    ),
  );
}

// ignore: non_constant_identifier_names
Widget SplashTxt() {
  // ignore: avoid_unnecessary_containers
  return Container(
    child: Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: const [
          Text(
            'Welcome',
            style: TextStyle(fontSize: 62, fontFamily: 'Risque'),
          ),
          Text(
            'Drop your craft',
            style: TextStyle(
              fontSize: 24,
            ),
          ),
        ],
      ),
    ),
  );
}
