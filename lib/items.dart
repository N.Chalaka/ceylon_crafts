import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'additems.dart';

final DatabaseReference database = FirebaseDatabase.instance.reference();

class Item extends StatefulWidget {
  const Item({Key? key}) : super(key: key);

  @override
  _ItemState createState() => _ItemState();
}

class _ItemState extends State<Item> {
  final addItemsRef = database.child('/items');

  navigateToAddItems() async {
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => const AddItem()));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Items'),
      ),
      body: SizedBox(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Center(
                  child: ElevatedButton(
                style: ElevatedButton.styleFrom(fixedSize: const Size(150, 50)),
                onPressed: () {
                  navigateToAddItems();
                },
                child: const Text('add items'),
              )),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Center(
                child: ElevatedButton(
                  style:
                      ElevatedButton.styleFrom(fixedSize: const Size(150, 50)),
                  onPressed: () {},
                  child: const Text('update items'),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Center(
                child: ElevatedButton(
                  style:
                      ElevatedButton.styleFrom(fixedSize: const Size(150, 50)),
                  onPressed: () {},
                  child: const Text('Delete items'),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
